{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RecordWildCards           #-}
{-# LANGUAGE TupleSections             #-}
module LLVM.Gen (
  -- * Monads
  execCS
  , runCS
  , CS
  , GlobalEnv
  , mkGEnv
  , execLS
  , askExtra
  , asksExtra
  , readExtra
  -- * Top definitions
  , declare
  , define
  , internalGlobalConstant
  , lookupFun
  , lookupRetType
  , defineStrings
  -- * Types
  , voidType
  , int64Type
  , doubleType
  , charType
  , bytePointerType
  , stringType
  , boolType
  , arrayOfType
  , arrayOfBaseType
  , isPointer
  , typedef
  -- * Blocks
  , startBlock
  , closeCurrentBlock
  , getCurrentLabel
  -- * Variables
  , lookupVar
  , withScope
  , declareVar
  , storeToVar
  , loadVar
  , storeToVarField
  , loadField
  , bindFields
  -- * Arrays
  , allocateArray
  , allocateStruct
  , loadArrayElem
  , loadArrayLen
  , storeArrayVarElem
  -- * Call
  , call
  , callVoid
  , ret
  , retVoid
  -- * Arithmetic and logic
  , add
  , fadd
  , sub
  , fsub
  , mul
  , fmul
  , sdiv
  , fdiv
  , srem
  , land
  , lor
  , lxor
  -- * Relations and branches
  , label
  , label'
  , icmp
  , fcmp
  , br
  , brCond
  -- * Pointers
  , isnull
  , nullptr
  , freePtr
  -- * Others
  , select
  , phi
  , unreachable
  , getStringPtr
  , getelementptr
  , getelementptrix
  , bitcast
  , internalVar
  -- * Debug
  , verbatim
  ) where

import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Bifunctor
import           Data.List
import           Data.Map             (Map)
import qualified Data.Map             as Map
import           Data.Maybe
import           Extra
import           LLVM.IR


data BlockBuilder = BlockBuilder {
  _id             :: Int
  , _instructions :: [Assigned Instruction]
  , _terminator   :: Maybe Terminator
  }
  deriving (Show)

data CS = CS {
  _labels              :: [Label]
  , _blockTable        :: Map Label BlockBuilder
  , _currentBlockLabel :: Label
  , _nextBlockId       :: Int
  , _varTable          :: Map String (Type, Value)
  }

data GlobalEnv a = GEnv {
  _funTable           :: Map String (Type, [Type])
  , _strTable         :: Map String (Type, Value)
  , _structTable      :: Map Value [(Type, String)] -- Value is the LVar with the type definition
  , _extra            :: a
  }

mkGEnv :: Map String (Type, [Type])
  -> Map String (Type, Value)
  -> Map Value [(Type, String)]
  -> a
  -> GlobalEnv a
mkGEnv _funTable _strTable _structTable _extra =
  GEnv {..}

askExtra :: MonadReader (GlobalEnv a) m => m a
askExtra = asks _extra

asksExtra :: MonadReader (GlobalEnv a) m => (a -> b) -> m b
asksExtra f = asks (f . _extra)

readExtra :: MonadReader (GlobalEnv a) m => Reader a r -> m r
readExtra f = runReader f <$> askExtra

-- | Lookup struct layout.
lookupStruct :: MonadReader (GlobalEnv a) m => Value -> m [(Type, String)]
lookupStruct struct = ($struct) <$> getLookupStruct

getLookupStruct :: MonadReader (GlobalEnv a) m => m (Value -> [(Type, String)])
getLookupStruct = do
  l <- asks _structTable
  return (\val -> fromMaybe (error ("struct " ++ show val ++ " not defined"))
           (Map.lookup val l))

lookupFun :: MonadReader (GlobalEnv a) m => String -> m (Type, [Type])
lookupFun fun = do
  r <- Map.lookup fun <$> asks _funTable
  return $ fromMaybe (error ("function " ++ fun ++ " not defined")) r

lookupRetType :: MonadReader (GlobalEnv a) m => String -> m Type
lookupRetType fun = fst <$> lookupFun fun

declare :: MonadWriter [TopDef] m => Type -> String -> [Argument] -> m ()
declare t fun args = tell1 (DeclareDef t fun args)

define :: MonadWriter [TopDef] m => Type -> String -> [Argument] -> [BasicBlock] -> m ()
define t fun args b = tell1 (DefineDef t fun args b)

internalGlobalConstant :: MonadWriter [TopDef] m => Value -> Type -> Value -> m ()
internalGlobalConstant var ty const = tell1 (DefineGlobal $ var := InternalConstant ty const)

-- usage: typedef "struct.Node" [i32, %struct.Node*]
typedef :: MonadWriter [TopDef] m => Value -> [Type] -> m ()
typedef typename fields =
  tell1 (DefineGlobal $ typename := TypeConstant (Struct fields))

execLS :: Writer [TopDef] a -> Module
execLS = Module . execWriter

emptyBlockBuilder :: Int -> BlockBuilder
emptyBlockBuilder idb = BlockBuilder idb [] Nothing

execCS :: State CS a -> [BasicBlock]
execCS = snd . runCS

runCS :: State CS a -> (a, [BasicBlock])
runCS = second (map buildBlock . sortOn (_id . snd) . Map.assocs . _blockTable) . (`runState`iniState)

defineStrings :: MonadWriter [TopDef] m => [String] -> m (Map String (Type, Value))
defineStrings strs = do
  sequence_ [ internalGlobalConstant var (arrTy (length str + 1)) (StringLit str0)
            | (str, (_, var)) <- assocs, let str0 = endnull str]
  return (Map.fromList assocs)
  where
    assocs = [ (str, (arrTy (length str + 1), var))
             | (str, var) <- zip strs vars, let str0 = endnull str]
    arrTy len = Array len charType
    vars = [ GVar $ "glob_str_" ++  show i | i <- [0..]]

bindFields :: (MonadReader (GlobalEnv a) m, MonadState CS m)
  => Bool -> Value -> Type -> m ()
bindFields hassuper thisptr classtype@(Declared classtypeval) = do
  ixfields <-
    (if hassuper then tail else id)
    . zip [0..]
    . fromMaybe (error $ "type not found" ++ show classtype)
    . Map.lookup classtypeval
    <$> asks _structTable
  mapM_ (bindField classtype thisptr) ixfields
bindFields _ _ _ = error "wrong type"

bindField :: MonadState CS m => Type -> Value -> (Int, (Type, String)) -> m ()
bindField (Declared classtypeval) ptr (ix, (fieldty, name)) = do
  fieldptr <- getelementptr (Declared classtypeval) ptr 0 ix
  bringVar fieldty name fieldptr
bindField _ _ _ = error "wrong type"

getStringPtr :: (MonadState CS m, MonadReader (GlobalEnv a) m) => String -> m (Type, Value)
getStringPtr str = do
  (arrTy, var) <- asks ((fromMaybe (error $ "did not find string " ++ str) . Map.lookup str) . _strTable)
  (stringType, ) <$> getelementptr arrTy var 0 0

iniState :: CS
iniState = CS {
  _labels = enumWords ['a'..'z']
  , _blockTable = Map.singleton entryLabel (BlockBuilder 0 [] Nothing)
  , _currentBlockLabel = entryLabel
  , _nextBlockId = 1
  , _varTable = mempty
  }

entryLabel :: Label
entryLabel = "entry"

fromPointer :: Type -> Type
fromPointer (Pointer t) = t
fromPointer _ = error "unexpected type"

label :: MonadState CS m => m Label
label = do
  cs <- get
  let Just (l, ls) = uncons (_labels cs)
  put cs {_labels = ls}
  return l

-- | Custom label
label' :: MonadState CS m => String -> m Label
label' str = ((str ++ "_") ++) <$> label

-- | Internal jvl variable name
internalVar :: MonadState CS f => String -> f String
internalVar str = (str ++ ) <$> label' "_internal_var"

newBlockId :: MonadState CS m => m Int
newBlockId = do
  cs <- get
  let i = _nextBlockId cs
  put cs {_nextBlockId = i + 1}
  return i

startBlock :: MonadState CS m => Label -> m ()
startBlock l = do
  i <- newBlockId
  let b = emptyBlockBuilder i
  setBlock b l
  setCurrentBlock l

setCurrentBlock :: MonadState CS m => Label -> m ()
setCurrentBlock l = modify $ \cs -> cs {_currentBlockLabel = l}

getBlock :: MonadState CS m => Label -> m BlockBuilder
getBlock l = fromJust . Map.lookup l <$> gets _blockTable

getCurrentLabel :: MonadState CS m => m Label
getCurrentLabel = withCurrent return

setBlock :: MonadState CS m => BlockBuilder -> Label -> m ()
setBlock b l = modify $ \cs -> cs {_blockTable = Map.insert l b (_blockTable cs)}

modifyBlock :: MonadState CS m => (BlockBuilder -> BlockBuilder) -> Label -> m ()
modifyBlock f l = modify $ \cs -> cs {_blockTable = Map.adjust f l (_blockTable cs)}

withCurrent :: MonadState CS m => (Label -> m a) -> m a
withCurrent = (gets _currentBlockLabel >>=)

closeBlock :: MonadState CS m => Terminator -> Label -> m ()
closeBlock term l = do
  b <- getBlock l
  let b'
        | Just _ <- _terminator b = b
        | otherwise = b {_terminator = Just term}
  setBlock b' l

buildBlock :: (Label, BlockBuilder) -> BasicBlock
buildBlock (l, b@BlockBuilder{..})
  | Nothing <- _terminator = error $ "block not closed: " ++ show b
  | Just t <- _terminator = BasicBlock l (reverse _instructions) t

closeCurrentBlock :: MonadState CS m => Terminator -> m ()
closeCurrentBlock = withCurrent . closeBlock

withScope :: MonadState CS m => m a -> m a
withScope ma = do
  s <- get
  ma <* put s

-- | Returns the pointer to the declared variable.
declareVar :: MonadState CS m => Type -> String -> m (Type, Value)
declareVar ty var = do
  ptr <- alloca1 ty
  insertVarToTable ty var (snd ptr)
  return ptr

insertVarToTable :: MonadState CS m => Type -> String -> Value -> m ()
insertVarToTable ty var ptr = do
  t <- gets _varTable
  let t' = Map.insert var (ty, ptr) t
  modify $ \cs -> cs {_varTable = t'}

-- | Brings a var into scope (the pointer must be already allocated).
--
-- If tyvar is T then ptr must have type Pointer T
bringVar :: MonadState CS m => Type -> String -> Value -> m ()
bringVar tyvar var ptr = insertVarToTable tyvar var ptr

alloca1 :: MonadState CS m => Type -> m (Type, Value)
alloca1 t = (Pointer t, ) <$> assign (Alloca1 t)

store :: MonadState CS m => (Type, Value) -> Value -> m ()
store (ty, val) ptr = emitVoid (Store ty val (Pointer ty) ptr)

-- %l = load i64* %ptr ; ptr has type i64*, returns (i64, %l)
load :: MonadState CS m => Type -> Value -> m (Type, Value)
load retty ptr = (retty, ) <$> assign (Load retty (Pointer retty) ptr)

lookupVar :: MonadState CS m => String -> m (Type, Value)
lookupVar var = do
  lu <- Map.lookup var <$> gets _varTable
  case lu of
    Just r@(_tyVar, _ptr) -> return r
    _                     -> error $ "Variable " ++ var ++ " not declared"

-- | var := val
storeToVar :: MonadState CS m => String -> Value -> m ()
storeToVar var val = do
  (tyVar, ptr) <- lookupVar var
  store (tyVar, val) ptr

-- | Node* var;
--
-- var->e = 1;
--
-- ptrvar = "list", field = "e", val = 1
--
-- Note that two loads and one store are required.
storeToVarField :: (MonadReader (GlobalEnv a) m, MonadState CS m)
  => String -> String -> (Type, Value) -> m ()
storeToVarField ptrvar field val = do
  struct <- loadVar ptrvar
  (tyfield, ptr) <- first fromPointer <$> getFieldPtr struct field
  when (tyfield /= fst val) (error "field type != val type")
  store val ptr

lookupFieldIx0 :: [(Type, String)] -> String -> (Int, Type)
lookupFieldIx0 fields field =
  let r = listToMaybe [ (ix, ty) | (ix, (ty, field')) <- zip [0..] fields, field' == field ]
  in fromMaybe (error "field not found") r

getFieldPtr :: (MonadState CS m, MonadReader (GlobalEnv a) m)
  => (Type, Value) -> String -> m (Type, Value)
getFieldPtr (tystructptr, structptr) field = case tystructptr of
  Pointer tystruct@(Declared tyref) -> do
    fields <- lookupStruct tyref
    let (ix, tyfi) = lookupFieldIx0 fields field
    (Pointer tyfi, ) <$> getelementptr tystruct structptr 0 ix
    where
  _   -> error $ "loadfield unexpected type: " ++ show tystructptr

loadField :: (MonadState CS m, MonadReader (GlobalEnv a) m)
  => (Type, Value) -> String -> m (Type, Value)
loadField struct@(_tystructptr, _structptr) field = do
  (tyfield, ptr) <- first fromPointer <$> getFieldPtr struct field
  load tyfield ptr

loadVar :: MonadState CS m => String -> m (Type, Value)
loadVar var = do
  (tyVar, ptr) <- lookupVar var
  load tyVar ptr

loadArrayElem :: MonadState CS m => (Type, Value) -> [(Type, Value)] -> m (Type, Value)
loadArrayElem var is = do
  (telem, ptrelem) <- first fromPointer <$> getArrayElemPtr var is
  load telem ptrelem

storeArrayVarElem :: MonadState CS m => String -> [(Type, Value)] -> Value -> m ()
storeArrayVarElem var is val = do
  tptr <- loadVar var
  storeArrayElem tptr is val

storeArrayElem :: MonadState CS m => (Type, Value) -> [(Type, Value)] -> Value -> m ()
storeArrayElem tptr is val = do
  (telem, ptrelem) <- first fromPointer <$> getArrayElemPtr tptr is
  store (telem, val) ptrelem


-- | It calculates the pointer to an element of an array. It only loads data
-- from memory if it has more than 1 dimension.
--
-- The first argument is the array.
-- The second argument are its indices.
getArrayElemPtr :: MonadState CS m => (Type, Value) -> [(Type, Value)] -> m (Type, Value)
getArrayElemPtr arr is = case is of
  [] -> error "no dimensions"
  [ix] -> getElemPtrAt ix
  (ix:ixs) -> do
      (tsubarr, ptrsubarr) <- first fromPointer <$> getElemPtrAt ix
      subarr <- load tsubarr ptrsubarr
      getArrayElemPtr subarr ixs
  where getElemPtrAt = getelementptrix arr 0 1
    -- NOTE: index 0: read this link:
    -- http://llvm.org/docs/GetElementPtr.html#what-is-the-first-index-of-the-gep-instruction
    -- NOTE: index 1 is the index to the actual array in the struct {i64, [0xt]}


-- | (arrayOfType telem, %val) -> [(int64, %dim)] -> ((arrayOfType telem), %ptr)
--
-- Allocates an array to the heap.
allocateArray :: (MonadReader (GlobalEnv a) m, MonadState CS m)
  => Type -> [(Type, Value)] -> m (Type, Value)
allocateArray _ [] = error "impossible"
allocateArray tyarr (dim:ds) = do
  num1 <- sizeof telem
  numArrBytes <- mul (IntLit num1) (snd dim)
  num2 <- sizeof int64Type
  totalBytes <- add (IntLit num2) numArrBytes
  -- totalBytes = 8 + dim * sizeof element. The 8 bytes are for the length.

  bytesptr <- callCalloc (int64Type, totalBytes)
  newArr <- bitcast bytesptr tyarr
  lenptr <- snd <$> getArrayLenPtr newArr
  store dim lenptr
  unless (null ds) $ do
    lcond <- label' "cond"
    lbody <- label' "body"
    lend <- label' "endwhile"
    (tint, ptrix) <- first fromPointer <$> alloca1 int64Type
    store (tint, IntLit 0) ptrix -- i = 0;
    br lcond

    startBlock lcond
    rix <- load tint ptrix
    rc <- snd <$> icmp SLT tint (snd rix) (snd dim) -- while (i < length)
    brCond rc lbody lend

    startBlock lbody
    allocateArray telem ds >>= storeArrayElem newArr [rix] . snd -- a[i] = new ...
    rix1 <- add (snd rix) (IntLit 1) -- i++;
    store (tint, rix1) ptrix
    br lcond

    startBlock lend

  return newArr
  where telem = arrayElementType tyarr

-- | Allocates a struct to the heap.
allocateStruct :: (MonadReader (GlobalEnv a) m, MonadState CS m) => Value -> m (Type, Value)
allocateStruct tyref = do
  fields <- lookupStruct tyref
  let fieldtys = map fst fields
  totalBytes <- IntLit . sum <$> mapM sizeof fieldtys
  bytesptr <- callCalloc (int64Type, totalBytes)
  bitcast bytesptr (Pointer (Declared tyref))

-- | Allocates the given number of bytes and sets them to 0.
callCalloc :: MonadState CS m => (Type, Value) -> m (Type, Value)
callCalloc bytes =
  call bytePointerType "mycalloc"
  [(int64Type, IntLit 1), bytes]

-- | Allocates the given number of bytes.
callMalloc :: MonadState CS m => (Type, Value) -> m (Type, Value)
callMalloc bytes = call bytePointerType allocfun [bytes]
  where
    allocfun = "mymalloc"

freePtr :: MonadState CS m => (Type, Value) -> m ()
freePtr ptr = do
  ptrbytes <- bitcast ptr bytePointerType
  callVoid "myfree" [ptrbytes]

-- | The argument must have type (arrayOfType sometype)
getArrayLenPtr :: MonadState CS m => (Type, Value) -> m (Type, Value)
getArrayLenPtr (tyarr, arr) = (int64Type, ) <$> getelementptr (unPointer tyarr) arr 0 0

-- | The argument must have type (arrayOfType sometype)
loadArrayLen :: MonadState CS m => (Type, Value) -> m (Type, Value)
loadArrayLen arr = do
  lenptr <- snd <$> getArrayLenPtr arr
  load int64Type lenptr

emit :: MonadState CS m => Assigned Instruction -> m ()
emit i = withCurrent (modifyBlock appendInstr)
  where
    appendInstr (BlockBuilder ix is t)
      | Phi{} <- unassign i, not (null is) = error "phi must be the first in a block"
      | otherwise = BlockBuilder ix (i:is) t

unassign :: Assigned a -> a
unassign (_ := a)       = a
unassign (NoAssigned a) = a

emitVoid :: MonadState CS m => Instruction -> m ()
emitVoid = emit . NoAssigned

voidType :: Type
voidType = VoidType

int64Type :: Type
int64Type = IntegerType 64

-- | indices must have type int32.
--
-- from the docs: When indexing into a (optionally packed) structure, only i32
-- integer constants are allowed ...
int32Type :: Type
int32Type = IntegerType 32

charType :: Type
charType = IntegerType 8

doubleType :: Type
doubleType = DoubleType

bytePointerType :: Type
bytePointerType = Pointer charType

-- | Returns the size in bytes.
--
-- Does not work for arrays because we always store 0 sized arrays although they
-- have more elements. e.g. [0 x int64]. Use pointers to arrays to avoid this
-- problem.
sizeof :: MonadReader (GlobalEnv a) m => Type -> m Int
sizeof t
  | Declared tyref <- t = do
      tys <- map fst . fromMaybe (error $ "type not found " ++ show tyref) . Map.lookup tyref
        <$> asks _structTable
      sizeof (Struct tys)
  | Struct ms <- t = sum <$> mapM sizeof ms
  | otherwise = return pt
  where
    pt
      | t == int64Type = 8
      | t == doubleType = 8
      | t == boolType = 1
      | isPointer t = 8
      | Array _ _ <- t = error "unknown size of array"
      | otherwise = error $ "incomplete sizeof: " ++ show t

isPointer :: Type -> Bool
isPointer t
  | Pointer _ <- t = True
  | otherwise = False

-- | Given the type an element returns the type of the array.
arrayOfType :: Type -> Type
arrayOfType t = Pointer (Struct [int64Type, Array 0 t])

-- | Given the basic type and the number of dimensions returns the type of the array.
arrayOfBaseType :: Type -> Int -> Type
arrayOfBaseType bt ndims
  | ndims <= 0 = error "ndims <= 0"
  | ndims == 1 = arrayOfType bt
  | otherwise = arrayOfType (arrayOfBaseType bt (ndims - 1))

-- | Given the type of the array returns the type of an element.
arrayElementType :: Type -> Type
arrayElementType (Pointer (Struct [ix, Array 0 t]))
  | ix == int64Type = t
arrayElementType t = error $ "wrong array type : " ++ show t

stringType :: Type
stringType = Pointer charType

boolType :: Type
boolType = IntegerType 1

fadd :: MonadState CS m => Value -> Value -> m Value
fadd a b = assign (FAdd DoubleType a b)

fsub :: MonadState CS m => Value -> Value -> m Value
fsub a b = assign (FSub DoubleType a b)

add :: MonadState CS m => Value -> Value -> m Value
add a b = assign (Add int64Type a b)

sub :: MonadState CS m => Value -> Value -> m Value
sub a b = assign (Sub int64Type a b)

mul :: MonadState CS m => Value -> Value -> m Value
mul a b = assign (Mul int64Type a b)

fmul :: MonadState CS m => Value -> Value -> m Value
fmul a b = assign (FMul DoubleType a b)

sdiv :: MonadState CS m => Value -> Value -> m Value
sdiv a b = assign (SDiv int64Type a b)

fdiv :: MonadState CS m => Value -> Value -> m Value
fdiv a b = assign (FDiv DoubleType a b)

srem :: MonadState CS m => Value -> Value -> m Value
srem a b = assign (SRem int64Type a b)

land :: MonadState CS m => Type -> Value -> Value -> m Value
land t a b = assign (And t a b)

lor :: MonadState CS m => Type -> Value -> Value -> m Value
lor t a b = assign (Or t a b)

lxor :: MonadState CS m => Type -> Value -> Value -> m Value
lxor t a b = assign (Xor t a b)

call :: MonadState CS m => Type -> String -> [(Type, Value)] -> m (Type, Value)
call t f args = (t, ) <$> assign (Call t f args)

isnull :: MonadState CS m => (Type, Value) -> m (Type, Value)
isnull (ty, ptr) = (boolType, ) <$> assign (ICmp Eq ty ptr NullPtr)

nullptr :: Type -> (Type, Value)
nullptr ptrty = (ptrty, NullPtr)

icmp :: MonadState CS m => IComparison -> Type -> Value -> Value -> m (Type, Value)
icmp cmp ty a b = (boolType, ) <$> assign (ICmp cmp ty a b)

fcmp :: MonadState CS m => FComparison -> Type -> Value -> Value -> m (Type, Value)
fcmp cmp ty a b = (boolType, ) <$> assign (FCmp cmp ty a b)

br :: MonadState CS m => Label -> m ()
br l = closeCurrentBlock (Br l)

brCond :: MonadState CS m => Value -> Label -> Label -> m ()
brCond val ltrue lfalse = closeCurrentBlock (BrCond val ltrue lfalse)

callVoid :: MonadState CS m => String -> [(Type, Value)] -> m ()
callVoid f args = emitVoid (Call VoidType f args)

ret :: MonadState CS m => (Type, Value) -> m ()
ret (ty, val) = closeCurrentBlock (Ret ty val)

retVoid :: MonadState CS m => m ()
retVoid = closeCurrentBlock RetVoid

select :: MonadState CS m => Value -> (Type, Value) -> (Type, Value) -> m Value
select c (t1, v1) (t2, v2) = assign (Select boolType c t1 v1 t2 v2)

phi :: MonadState CS m => Type -> [(Value, Label)] -> m (Type, Value)
phi t assocs = (t, ) <$> assign (Phi t assocs)

unreachable :: MonadState CS m => m ()
unreachable = emitVoid Unreachable

getelementptr :: MonadState CS m => Type -> Value -> Int -> Int -> m Value
getelementptr structty structptr ix1 ix2 = assign (
  Getelementptr structty structptr
    int32Type (IntLit ix1) int32Type (IntLit ix2))

getelementptrix :: MonadState CS m =>
  (Type, Value) -> Int -> Int -> (Type, Value) -> m (Type, Value)
getelementptrix (tyarr, arr) ix1 ix2 (t3, ix3) = (Pointer (arrayElementType tyarr), )
  <$> assign (Getelementptrix (unPointer tyarr) tyarr arr
              int32Type (IntLit ix1) int32Type (IntLit ix2) t3 ix3)

unPointer :: Type -> Type
unPointer (Pointer t) = t
unPointer _           = error "unpointer"

-- | YOLO type cast. I only use it for malloc/calloc
bitcast :: MonadState CS m => (Type, Value) -> Type -> m (Type, Value)
bitcast (t1, v1) t2 = (t2, ) <$> assign (Bitcast t1 v1 t2)

verbatim :: MonadState CS m => String -> m ()
verbatim = emitVoid . Verbatim

newLocal :: MonadState CS m => m Value
newLocal = LVar <$> label

assign :: MonadState CS m => Instruction -> m Value
assign i = do
  r <- newLocal
  emit (r := i)
  return r
