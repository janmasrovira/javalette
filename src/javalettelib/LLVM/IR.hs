module LLVM.IR where

import Extra

data Type =
  IntegerType Int
  | VoidType
  | DoubleType
  | Pointer Type
  | Array Int Type
  | Struct [Type]
  | Declared Value
  deriving (Eq, Show)

data Value =
  LVar String
  | GVar String
  | IntLit Int
  | DoubleLit Double
  | StringLit String
  | NullPtr
  deriving (Show, Eq, Ord)

type Label = String
type Argument = (Type, Value)

data BasicBlock =
  BasicBlock Label [Assigned Instruction] Terminator
  deriving (Show)

data TopDef =
  DefineDef Type String [Argument] [BasicBlock]
  | DeclareDef Type String [Argument]
  | DefineGlobal (Assigned Instruction)
  deriving (Show)

newtype Module = Module [TopDef]
  deriving (Show)

data Assigned a =
  Value := a
  | NoAssigned a
  deriving (Show)

data Instruction =
  Call Type String [(Type, Value)]
  | Alloca1 Type
  | Store Type Value Type Value
  | Load Type Type Value
  | Add Type Value Value
  | Sub Type Value Value
  | Mul Type Value Value
  | SDiv Type Value Value
  | SRem Type Value Value
  | FAdd Type Value Value
  | FSub Type Value Value
  | FMul Type Value Value
  | FDiv Type Value Value
  | ICmp IComparison Type Value Value
  | FCmp FComparison Type Value Value
  | And Type Value Value
  | Or Type Value Value
  | Xor Type Value Value
  | Getelementptr Type Value Type Value Type Value
  | Getelementptrix Type Type Value Type Value Type Value Type Value
  | InternalConstant Type Value
  | Select Type Value Type Value Type Value
  | Phi Type [(Value, Label)]
  | Unreachable
  | Bitcast Type Value Type
  | TypeConstant Type
  | Extractvalue Type Value Int
  | Insertvalue Type Value Type Value Int
  -- DEBUG
  | Verbatim String
  deriving (Show)

data Terminator =
  RetVoid
  | Ret Type Value
  | BrCond Value Label Label
  | Br Label
  deriving (Show)

data IComparison =
  Eq
  | NotEq
  | SGT
  | SLT
  | SLE
  | SGE
  deriving (Show)

data FComparison =
  OEq
  | ONE
  | OGT
  | OGE
  | OLT
  | OLE
  deriving (Show)

ppModule :: Module -> String
ppModule (Module ds) = unlines (map ppTopDef ds)

ppTopDef :: TopDef -> String
ppTopDef td = case td of
  DeclareDef ty fun args -> ppHeader "declare" ty fun args
  DefineDef ty fun args blocks -> unlines [ppHeader "define" ty fun args, ppBlocks blocks]
  DefineGlobal def -> ppAssigned ppInstruction def
  where
    ppBlocks = unlines . (\x -> ["{"] ++ x ++ ["}"]) . map ppBlock
    ppBlock (BasicBlock l body term) = unlines
      [ppLabel l, inden 2 $ ppBody body, inden 2 $ ppTerm term]
    ppTerm t = case t of
      Br l -> unwords ["br", ppTypeLabelRef l]
      BrCond c l1 l2 -> unwords
        ["br", "i1", ppValue c, ",", ppTypeLabelRef l1, ",", ppTypeLabelRef l2]
      RetVoid -> "ret void"
      Ret t v -> unwords ["ret", ppType t, ppValue v]
    ppLabel l = l ++ ":"
    ppBody body = unlines (map (ppAssigned ppInstruction) body)
    ppHeader decl ty fun args = unwords [decl, ppType ty, '@':fun, ppArgumentsParens args]

ppTypeLabelRef :: Label -> String
ppTypeLabelRef l = unwords ["label", ppLabelRef l]

ppLabelRef :: Label -> String
ppLabelRef l = '%':l

endnull :: String -> String
endnull = (++"\\00")

ppArgument :: Argument -> String
ppArgument (ty, arg) = unwords [ppType ty, ppValue arg]

ppArguments :: [Argument] -> String
ppArguments args = commaSep (map ppArgument args)

ppArgumentsParens :: [Argument] -> String
ppArgumentsParens = parens . ppArguments

ppValue :: Value -> String
ppValue v = case v of
  IntLit i      -> show i
  LVar x        -> '%':x
  GVar x        -> '@':x
  DoubleLit d   -> show d
  StringLit str -> 'c':'"':str ++ "\""
  NullPtr       -> "null"

ppType :: Type -> String
ppType t = case t of
  VoidType      -> "void"
  IntegerType n -> "i" ++ show n
  DoubleType    -> "double"
  Pointer t'    -> ppType t' ++ "*"
  Array k e     -> "[" ++ show k ++ " x " ++ ppType e ++ "]"
  Struct ts     -> "{" ++  commaSep (map ppType ts) ++ "}"
  Declared val  -> ppValue val

ppAssigned :: (a -> String) -> Assigned a -> String
ppAssigned pp a = case a of
  var := x     -> unwords [ppValue var, "=", pp x]
  NoAssigned x -> pp x

ppInstruction :: Instruction -> String
ppInstruction i = case i of
  Alloca1 ty -> unwords ["alloca", ppType ty]
  Store t val tptr ptr -> unwords
    ["store", ppType t, ppValue val, ",", ppType tptr, ppValue ptr]
  Load t tptr ptr -> unwords ["load", ppType t, ",", ppType tptr, ppValue ptr]
  Add t a b -> binop "add" t a b
  Sub t a b -> binop "sub" t a b
  Mul t a b -> binop "mul" t a b
  SDiv t a b -> binop "sdiv" t a b
  SRem t a b -> binop "srem" t a b
  FAdd t a b -> binop "fadd" t a b
  FSub t a b -> binop "fsub" t a b
  FMul t a b -> binop "fmul" t a b
  FDiv t a b -> binop "fdiv" t a b
  And t a b -> binop "and" t a b
  Or t a b -> binop "or" t a b
  Xor t a b -> binop "xor" t a b
  ICmp c t a b -> unwords ["icmp", ppIComparison c, ppType t, ppValue a, ",", ppValue b]
  FCmp c t a b -> unwords ["fcmp", ppFComparison c, ppType t, ppValue a, ",", ppValue b]
  Call t fun args -> unwords ["call", ppType t, ppValue (GVar fun),
                              parens (commaSep (map ppTypeValue args))]
  Unreachable -> "unreachable"
  Getelementptr ty ptr t1 ix1 t2 ix2 -> unwords [
    "getelementptr", ppType ty, ",", ppTypeValue (Pointer ty, ptr), ","
    , ppTypeValue (t1, ix1), ",", ppTypeValue (t2, ix2)]
  Getelementptrix ty typtr ptr t1 ix1 t2 ix2 t3 ix3 -> unwords [
    "getelementptr", ppType ty, ",", ppTypeValue (typtr, ptr), ",", ppTypeValue (t1, ix1)
    , ",", ppTypeValue (t2, ix2), ",", ppTypeValue (t3, ix3)]
  InternalConstant t v -> unwords ["internal", "constant", ppTypeValue (t, v)]
  Select tc c t1 v1 t2 v2 -> unwords ["select", ppTypeValue (tc, c), ",",
                                     ppTypeValue (t1, v1), ",", ppTypeValue (t2, v2)]
  Phi t items -> unwords ["phi", ppType t, ppitems items]
    where
      ppitems = commaSep . map ppitem
      ppitem (val, lab) = unwords ["[", ppValue val, ",", ppLabelRef lab, "]"]
  Bitcast t1 v1 t2 -> unwords ["bitcast", ppTypeValue (t1, v1), "to", ppType t2]
  TypeConstant ty -> unwords ["type", ppType ty]
  Insertvalue tyagg agg tyval val ix ->
    unwords ["insertvalue", ppTypeValue (tyagg, agg), ","
            , ppTypeValue (tyval, val), ",", show ix]
  Extractvalue tyagg agg ix ->
    unwords ["extractvalue", ppTypeValue (tyagg, agg), ",", show ix]
  Verbatim v -> v
  where
    binop op t a b = unwords [op, ppType t, ppValue a, ",", ppValue b]

ppTypeValue :: (Type, Value) -> String
ppTypeValue (t, v) = unwords [ppType t, ppValue v]

ppIComparison :: IComparison -> String
ppIComparison c = case c of
  Eq    -> "eq"
  NotEq -> "ne"
  SGT   -> "sgt"
  SLT   -> "slt"
  SLE   -> "sle"
  SGE   -> "sge"

ppFComparison :: FComparison -> String
ppFComparison c = case c of
  OEq -> "oeq"
  ONE -> "one"
  OGT -> "ogt"
  OGE -> "oge"
  OLT -> "olt"
  OLE -> "ole"
