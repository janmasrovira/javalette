module Extra where

import           Control.Monad.Except
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.List
import           Data.List.Extra
import           Data.Map             (Map)
import qualified Data.Map             as Map

-- | Inserts a new element. Also returns the replaced element, if any.
insertLookup :: Ord k => k -> a -> Map k a -> (Maybe a, Map k a)
insertLookup = Map.insertLookupWithKey (\_ a _ -> a)

-- | Analogous to 'either'.
except :: (e -> b) -> (a -> b) -> Except e a -> b
except f g = either f g . runExcept

-- | Recover form an exception.
fromExcept :: (e -> a) -> Except e a -> a
fromExcept f = either f id . runExcept

-- | Discards state changes.
localState :: MonadState s m => m a -> m a
localState ma = do
  s <- get
  r <- ma
  put s
  return r

-- | Lookup for a stack of maps.
lookupMaps :: Ord k => k -> [Map k v] -> Maybe v
lookupMaps k = firstJust (Map.lookup k)

commaSep :: [String] -> String
commaSep = intercalate ", "

parens :: String -> String
parens s = "(" ++ s ++ ")"

enumWords :: [a] -> [[a]]
enumWords a = go [[]]
  where go lshorter = let t = [ x:shorter | x <- a, shorter <- lshorter ]
                     in t ++ go t

tell1 :: (MonadWriter (f a) m, Applicative f) => a -> m ()
tell1 = tell . pure

inden :: Int -> String -> String
inden k = unlines . map (replicate k ' ' ++) . lines

indenLines :: Int -> [String] -> [String]
indenLines k = map (replicate k ' ' ++)

iteratek :: Int -> (a -> a) -> a -> a
iteratek k f x0 = iterate f x0 !! k

iteratekM :: Monad m => Int -> (a -> m a) -> a -> m a
iteratekM 0 _ x0 = return x0
iteratekM n f x0 = f x0 >>= iteratekM (n - 1) f

unsafe :: Show e => Except e a -> a
unsafe = either (error . show) id . runExcept

consMaybe :: Maybe a -> [a] -> [a]
consMaybe (Just a) = (a:)
consMaybe _ = id

exceptCatch :: MonadError e' m => (e -> m a) -> Except e a -> m a
exceptCatch fe = except fe return

fromMaybeM :: Monad m => m a -> m (Maybe a) -> m a
fromMaybeM ma x = x >>= maybe ma return
