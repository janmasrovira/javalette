module Javalette.Parser (
  parse
  , module Javalette.Grammar.AbsJavalette
  ) where

import Javalette.Grammar.AbsJavalette
import Javalette.Grammar.ErrM
import Javalette.Grammar.ParJavalette
import Control.Monad.Except

-- | Returns the AST of a Javalette program or a parser error.
parse :: String -> Except String Program
parse str = case pProgram (myLexer str) of
  Bad e -> throwError e
  Ok r  -> return r
