{-# LANGUAGE RecordWildCards #-}
module Javalette.Main where

import Control.Monad.Extra
import Data.Maybe
import Data.Monoid
import Extra
import Javalette.Parser
import Javalette.TypeChecker
import Options.Applicative
import System.Exit
import System.FilePath
import System.IO
import System.Process

import Javalette.CodeGenerator
import Javalette.Paths
import LLVM.IR

data CLIOpts = Opts {
  _srcPath    :: FilePath
  , _outName  :: Maybe String
  , _buildExe :: Bool
  , _debug    :: Bool
  , _noBuiltinLists :: Bool
  }


optsInfo :: Parser CLIOpts
optsInfo = Opts
  <$> argument str (metavar "FILE")
  <*> optional (strOption (
    short 'o'
    <> metavar "STRING"
    <> help "Output name"))
  <*> switch (
    short 'x'
    <> help "Output executable file")
  <*> switch (
    short 'd'
    <> help "Show debug mesages")
  <*> switch (
    long "no-lists"
    <> help "Do not include builtin lists")

parOpts :: ParserInfo CLIOpts
parOpts = info (optsInfo <**> helper)
      ( fullDesc
     <> progDesc "Compile a Javalette source file"
     <> header "jlc - The Javalette compiler" )

main :: IO ()
main = do
  Opts{..} <- execParser parOpts
  let
    name = fromMaybe (takeFileName _srcPath) _outName
    outf ext = replaceFileName _srcPath name -<.> ext
    llpath = outf ".ll"
    bcpath = outf ".bc"
    exepath = outf ".x"
    linkedbc = outf ".ln.bc"
  res <- parse <$> readFile _srcPath
  prog <- except jvlError return res
  when _debug (putStrLn "parsed Program" >> print prog)
  (defstable, checkedProgram) <- except jvlErrorShow return (checkProgram _noBuiltinLists prog)
  let llcode = ppModule (codeGen defstable checkedProgram)
  writeFile llpath llcode
  when _debug (putStrLn llcode)
  hPutStr stderr $ unlines ["OK"]
  llvmCompileBc llpath bcpath
  when _buildExe (compileExe bcpath linkedbc exepath)
  exitSuccess
  where
    compileExe bcpath linkedbc exepath = do
      llvmLinkRuntime bcpath linkedbc
      llvmCompile linkedbc exepath
    jvlErrorShow :: Show e => e -> IO a
    jvlErrorShow err = jvlError (show err)
    jvlError :: String -> IO a
    jvlError err = do
        hPutStr stderr $ unlines ["ERROR", err]
        exitFailure

llvmCompileBc :: FilePath -> FilePath -> IO ()
llvmCompileBc llc bcpath =
  callProcess llvm_as [llc, "-o", bcpath]

llvmLinkRuntime :: FilePath -> FilePath -> IO ()
llvmLinkRuntime bcpath linkedbc =
  callProcess llvm_link [bcpath, runtimebc, "-o", linkedbc]

llvmCompile :: FilePath -> FilePath -> IO ()
llvmCompile linkedbc exec = do
  callProcess llc [linkedbc, "-filetype=obj", "-o", obj]
  callProcess "gcc" [obj, "-o", exec, "-no-pie"]
    where
      obj = replaceExtension linkedbc ".o"
