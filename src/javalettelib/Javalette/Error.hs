{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
module Javalette.Error where

import           Control.Monad.Except
import           Javalette.Grammar.AbsJavalette

data Error =
  Internal String
 | FunNotDef Ident
 | DupFunDef Ident
 | DupVarDef Ident
 | VarNotDef Ident
 | MainNotDef
 | WrongTypeExpect Type Expr Type -- expected expr found
 | WrongType Expr String
 | VoidRet
 | WrongArgs Ident [[Type]] [Type]
 | MissingReturn Ident
 | ExpectedArray Expr
 | ExpectedPointerTy (Maybe Expr) Type
 | DupTypedef Ident
 | DupField Ident
 | DupStructDef Ident
 | GenericError String
 | FieldNotDef Ident
 | StructNotDef Ident
 | TemplateError String
 deriving (Show)


errDupFunDef :: MonadError Error m => Ident -> m a
errDupFunDef = throwError . DupFunDef

errDupField :: MonadError Error m => Ident -> m a
errDupField = throwError . DupField

errDupStructDef :: MonadError Error m => Ident -> m a
errDupStructDef = throwError . DupStructDef

errDupTypedef :: MonadError Error m => Ident -> m a
errDupTypedef = throwError . DupTypedef

errDupVarDef :: MonadError Error m => Ident -> m a
errDupVarDef = throwError . DupVarDef

errFunNotDef :: MonadError Error m => Ident -> m a
errFunNotDef = throwError . FunNotDef

errStructNotDef :: MonadError Error m => Ident -> m a
errStructNotDef = throwError . StructNotDef

errFieldNotDef :: MonadError Error m => Ident -> m a
errFieldNotDef = throwError . FieldNotDef

errMainNotDef :: MonadError Error m => m a
errMainNotDef = throwError MainNotDef

errVarNotDef :: MonadError Error m => Ident -> m a
errVarNotDef = throwError . VarNotDef

errBadArgs :: MonadError Error m => Ident -> [[Type]] -> [Type] -> m a
errBadArgs fun expected given =
  throwError (WrongArgs fun expected given)

errWrongTypeExpect :: MonadError Error m => Type -> Expr -> Type -> m a
errWrongTypeExpect expected e found = throwError (WrongTypeExpect expected e found)

errWrongType :: MonadError Error m => Expr -> String -> m a
errWrongType expr msg = throwError (WrongType expr msg)

errNoArray :: MonadError Error m => Expr -> m a
errNoArray = throwError . ExpectedArray

errNoPointer :: MonadError Error m => Maybe Expr -> Type -> m a
errNoPointer e = throwError . ExpectedPointerTy e

errVoidRet :: MonadError Error m => m a
errVoidRet = throwError VoidRet

errMissingRet :: MonadError Error m => Ident -> m a
errMissingRet = throwError . MissingReturn

errGeneric :: MonadError Error m => String -> m a
errGeneric = throwError . GenericError

errTemplate :: MonadError Error m => String -> m a
errTemplate = throwError . TemplateError
