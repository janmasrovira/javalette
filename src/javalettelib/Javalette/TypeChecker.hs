{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE ViewPatterns        #-}
module Javalette.TypeChecker where

import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.Extra
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Generics.Uniplate.Data
import           Data.List.Extra
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe
import           Data.Set                       (Set)
import qualified Data.Set                       as Set
import           Extra
import           Javalette.Builtin.List
import           Javalette.Common
import           Javalette.Error
import           Javalette.Grammar.AbsJavalette
import           Javalette.Template

import           Debug.Trace

type VarTable = [Map Ident Type]
type VarLookup = Ident -> Maybe Type
type Level = Int

newtype CheckedProgram = Checked Program
  deriving (Show)

data MethodCtx = MethodCtx {
  _class       :: Maybe Ident
  , _defsTable :: DefsTable
  }

data MethodBodyCtx = MethodBodyCtx {
  _methodCtx :: MethodCtx
  , _retType :: Type
  }

data ExprCtx = ExprCtx {
  _methodBodyCtx :: MethodBodyCtx
  , _varlookup   :: VarLookup
  }

newtype TypedExpr = TypedExpr (Type, Expr)

typedExpr :: TypedExpr -> Expr
typedExpr (TypedExpr (t, e)) = ETyExpr t e


addVar_mtd :: (MonadReader MethodBodyCtx m, MonadState VarTable m, MonadError Error m)
  => Type -> Ident -> m ()
addVar_mtd t0 var = do
  ft <- asks (_defsTable . _methodCtx)
  runReaderT (addVar t0 var) ft


addVar_expr :: (MonadState VarTable m, MonadReader ExprCtx m, MonadError Error m)
  => Type -> Ident -> m ()
addVar_expr t0 var = do
  c <- asks (_defsTable . _methodCtx . _methodBodyCtx)
  runReaderT (addVar t0 var) c

-- | Add a variable to the table.
addVar :: (MonadReader DefsTable m, MonadState VarTable m, MonadError Error m)
  => Type -> Ident -> m ()
addVar t var = do
  vt <- get
  let vt0 = head vt
      insertVar (vt0:vt) = put $ Map.insert var t vt0 : vt
      insertVar _        = error "empty VarTable stack"
  case Map.lookup var vt0 of
    Nothing -> insertVar vt
    Just _  -> errDupVarDef var

isClass :: MonadReader DefsTable m => Ident -> m Bool
isClass cl = asks (Map.member cl . _classTable)

-- | Create a function lookup from a table.
varLookup :: MonadState VarTable m => m VarLookup
varLookup = do
  vt <- get
  return (`lookupMaps` vt)

-- | Checks that a program is type-correct.
checkProgram :: MonadError Error m => Bool -- ^ If 'True', builtin lists are excluded.
  -> Program -- ^ 'Program' to check.
  -> m (DefsTable, CheckedProgram)
checkProgram excludeLists p0 = do
  p1@(Prog ds) <-
    return .
    (if excludeLists then id else builtinListsTransform)
    >=> expandTemplates
    >=> substAliasProg
    >=> subsObjTypesToPointersProg
    $ p0
  ft1 <- buildDefsTable p1
  flip runReaderT ft1 $ do
    checkMainExists
    cds <- mapM checkTopDef ds
    return (ft1, Checked (Prog cds))

checkTopDef :: (MonadReader DefsTable m, MonadError Error m) => TopDef -> m TopDef
checkTopDef (noTemplateDef -> def) = NoTemplateDef <$> case def of
  TopFunDef{} -> checkTopFunDef def
  ClassDef{}  -> checkClassDef def
  StructDef{} -> return def
  TypeDef{}   -> return def

-- | Checks that the main function is defined.
checkMainExists :: (MonadReader DefsTable m, MonadError Error m) => m ()
checkMainExists = do
  ft <- asks _funTable
  case Map.lookup mainFun ft of
    Just (Int, []) -> return ()
    _              -> errMainNotDef
  where mainFun = Ident "main"


checkTopFunDef :: (MonadReader DefsTable m, MonadError Error m) => IdentDef -> m IdentDef
checkTopFunDef tdef = do
  funt <- ask
  let methodctx = MethodCtx {
        _class = Nothing
        , _defsTable = funt
        }
  case tdef of
    TopFunDef def -> TopFunDef <$> runReaderT (checkFunDef def) methodctx
    _             -> return tdef

-- | Checks that a definition is type-correct.
checkFunDef :: forall m. (MonadReader MethodCtx m, MonadError Error m)
  => FnDef -> m FnDef
checkFunDef = checkFunDef' []

checkFunDef' :: forall m. (MonadReader MethodCtx m, MonadError Error m)
  => [(Type, Ident)] -> FnDef -> m FnDef
checkFunDef' inivars (FunDef ret fun args (Block s)) = do
  methodctx <- ask
  let bodyctx = MethodBodyCtx methodctx ret
  cs <- flip evalStateT [mempty] $ flip runReaderT bodyctx $ do
      mapM_ (uncurry addVar_mtd) inivars
      withNewLevel $ do
        mapM_ addArg args
        checkStmts s
  unless (ret == Void) (checkReachRet fun s)
  return  (FunDef ret fun args (Block cs))
    where
      addArg (Argument (noTemplate -> t) var) = addVar_mtd t var
checkFunDef' _ _ = error "template fun"

-- | Checks that a sequence of statements are type-correct.
checkStmts :: (MonadState VarTable m, MonadReader MethodBodyCtx m, MonadError Error m)
  => [Stmt] -> m [Stmt]
checkStmts = mapM checkStmt

checkClassDef :: forall m. (MonadReader DefsTable m, MonadError Error m) => IdentDef -> m IdentDef
checkClassDef (ClassDef nm items) = do
  defs <- ask
  let ctx = MethodCtx { _class = Just name, _defsTable = defs }
      fields = maybe (error "internal: class not found") _fields (Map.lookup name (_classTable defs))
      chkItem :: ClassItem -> m ClassItem
      chkItem ci = case ci of
        m@ClassMethod{} -> funToMethod <$> runReaderT (checkFunDef' fields (methodToFun m)) ctx
        _ -> return ci
  chkitems <- mapM chkItem items
  return (ClassDef nm chkitems)
  where
    name = case nm of
      Parent a _ -> a
      NoParent a -> a
checkClassDef td = return td

-- | Checks that each branch has a return.
checkReachRet :: forall m. MonadError Error m => Ident -> [Stmt] -> m ()
checkReachRet fun ss = unless (any reachRet ss) (errMissingRet fun)

reachRet :: Stmt -> Bool
reachRet s = case s of
  Return _            -> True
  BStmt (Block b) -> any reachRet b
  CondElse c a b -> case staticEvalBool c of
    Nothing    -> all reachRet [a, b]
    Just True  -> reachRet a
    Just False -> reachRet b
  Cond (staticEvalBool -> Just True) a -> reachRet a
  While (staticEvalBool -> Just True) a -> reachRet a
  _                -> False

staticEvalBool :: Expr -> Maybe Bool
staticEvalBool e = case e of
  ELitTrue    -> Just True
  ELitFalse   -> Just False
  ETyExpr _ a -> staticEvalBool a
  _           -> Nothing

-- | Checks that a statement is type-correct.
checkStmt :: forall m. (MonadState VarTable m, MonadReader MethodBodyCtx m, MonadError Error m)
  => Stmt -> m Stmt
checkStmt = go
  where
    go :: Stmt -> m Stmt
    go s =
      case s of
        Empty -> return s
        SExp e -> SExp <$> chkExpr Void e
        BStmt (Block b) -> BStmt . Block <$> gos1 b
        Decl (noTemplate -> ty) itms -> Decl (NoTemplate ty) <$> mapM checkItem itms
          where checkItem itm =
                  case itm of
                    NoInit var -> addVar_mtd ty var >> return itm
                    Init var e -> do
                      ce <- chkExpr ty e
                      addVar_mtd ty var
                      return $ Init var ce
        Ass var e -> do
          vl <- varLookup
          structs <- asks (_structTable . _defsTable . _methodCtx)
          ty <- lookupLhsType structs vl var
          Ass var <$> chkExpr ty e
        VReturn -> do
          ret <- asks _retType
          if  ret == Void
            then return s
            else errVoidRet
        Return e -> do
          ret <- asks _retType
          Return <$> chkExpr ret e
        Cond c b -> do
          cc <- checkCond c
          cb <- go1 b
          return $ Cond cc cb
        CondElse c s1 s2 -> do
          cc <- checkCond c
          cs1 <- go1 s1
          cs2 <- go1 s2
          return $ CondElse cc cs1 cs2
        While c b -> do
          cc <- checkCond c
          cb <- go1 b
          return $ While cc cb
        Incr var -> do
          vl <- varLookup
          typ <- lookupVarType vl var
          go (desugarIncr typ var)
        Decr var -> do
          vl <- varLookup
          typ <- lookupVarType vl var
          go (desugarDecr typ var)
        SIter t var arr s1 -> do
          carr <- chkExpr (Array t) arr
          withNewLevel $ do
            addVar_mtd (noTemplate t) var
            cs1 <- go s1
            return $ SIter t var carr cs1
      where
        getExprEnv = do
          vl <- varLookup
          ft <- ask
          return (ExprCtx ft vl)
        checkCond = chkExpr Bool
        chkExpr :: Type -> Expr -> m Expr
        chkExpr ty e = do
          env <- getExprEnv
          runReaderT (checkExpr ty e) env
        oneOfType typ =
          case typ of
            Double -> ELitDouble 1
            Int    -> ELitInt 1
            _      -> error $ show typ ++ " is not a numeric type"
        desugarIncr ty var = Ass (LhsIdent var) (EAdd (EVar var) Plus (oneOfType ty))
        desugarDecr ty var = Ass (LhsIdent var) (EAdd (EVar var) Minus (oneOfType ty))
        go1 = withNewLevel . go
        gos1 = withNewLevel . mapM go

withNewLevel :: MonadState VarTable m => m a -> m a
withNewLevel ma = pushLevel >> ma <* popLevel
  where
    pushLevel, popLevel :: MonadState VarTable m => m ()
    pushLevel = modify (mempty:)
    popLevel = modify tail

-- | Casts the expression if necessary
castMaybe :: forall m. (MonadReader ExprCtx m, MonadError Error m) =>
  Type -> TypedExpr -> m TypedExpr
castMaybe tytarget ce@(TypedExpr (ty0, e0)) =
  if ty0 == tytarget
    then return ce
    else do
    unlessM (tytarget >== ty0) (errWrongTypeExpect tytarget e0 ty0)
    return $ cast ce tytarget

cast :: TypedExpr -> Type -> TypedExpr
cast te@(TypedExpr (t0, _)) t1 = TypedExpr (t1, ETyExpr t1 (ETyCast t0 (typedExpr te) t1))

-- | Checks that an expression is type-correct and it has the given type.
checkExpr :: (MonadState VarTable m, MonadReader ExprCtx m, MonadError Error m) => Type -> Expr -> m Expr
checkExpr t e = typedExpr <$> checkExpr_ t e

checkExpr_ :: (MonadState VarTable m, MonadReader ExprCtx m, MonadError Error m) => Type -> Expr -> m TypedExpr
checkExpr_ tytarget = inferExpr_ >=> castMaybe tytarget

-- | Lookup the type of a variable.
lookupVarType :: MonadError Error m => VarLookup -> Ident -> m Type
lookupVarType vl var = maybe (errVarNotDef var) return (vl var)


lookupLhsType :: MonadError Error m => StructTable -> VarLookup -> Lhs -> m Type
lookupLhsType structs vl lhs = case lhs of
  LhsIdent var     -> lookupVarType vl var
  LhsArray var ixs -> lookupIxArrayType vl var ixs
  LhsPtr var field -> do
    varty <- lookupLhsType structs vl (LhsIdent var)
    case varty of
      Pointer (NoTemplate (CustomTy struct)) -> lookupStructFieldType structs struct field
      t                         -> errNoPointer Nothing t

lookupIxArrayType :: MonadError Error m => VarLookup -> Ident -> [a] -> m Type
lookupIxArrayType vl var ixs = do
  t <- lookupVarType vl var
  let it = iteratekM (length ixs) unArrayType t
  maybe (errGeneric "wrong too many indices") return it

lookupStructFieldType :: MonadError Error m => StructTable -> Ident -> Ident -> m Type
lookupStructFieldType tbl struct field = do
  s <- lookupStruct tbl struct
  maybe (errFieldNotDef field) return (Map.lookup field s)

lookupStruct :: MonadError Error m => StructTable -> Ident -> m (Map Ident Type)
lookupStruct t struct =
  maybe (errStructNotDef struct) return (Map.lookup struct t)

arrayElementType :: Type -> Int -> Maybe Type
arrayElementType tyarr dims = iteratekM dims unArrayType tyarr

-- | Infers and annotates an expression with its type.
inferExpr :: forall m. (MonadState VarTable m, MonadReader ExprCtx m, MonadError Error m) => Expr -> m Expr
inferExpr e' = typedExpr <$> inferExpr_ e'

inferExpr_ :: forall m. (MonadState VarTable m, MonadReader ExprCtx m, MonadError Error m) => Expr -> m TypedExpr
inferExpr_ e' = case e' of
  ELitTrue     -> return (type_ Bool)
  ELitFalse    -> return (type_ Bool)
  ELitInt _    -> return (type_ Int)
  ELitDouble _ -> return (type_ Double)
  EString _ -> return (type_ String)
  ENullPtr (noTemplate -> ty) -> case ty of
    Pointer{} -> return (type_ ty)
    _         -> errNoPointer (Just e') ty
  ENew (noTemplate -> ty) -> return (type_ (Pointer (NoTemplate ty)))
  EVar var -> do
    vl <- asks _varlookup
    type_ <$> lookupVarType vl var
  EFree eptr -> do
    ce@(TypedExpr (t, e)) <- inferExpr_ eptr
    unless (isPointerType t) (errNoPointer (Just eptr) t)
    return (TypedExpr (Void, EFree (typedExpr ce)))
  EApp f args -> do
    ft <- asks (_funTable . _defsTable . _methodCtx . _methodBodyCtx)
    (ret, tyargs) <- lookupFunction ft f
    unless (length tyargs == length args) (errGeneric "wrong number of arguments")
    cargs <- zipWithM checkExpr tyargs args
    return (TypedExpr (ret, EApp f cargs))
  EMul e1 op e2 -> binNumOp (`EMul` op) (Right op) e1 e2
  EAdd e1 op e2 -> binNumOp (`EAdd` op) (Left op) e1 e2
  Neg e -> unNumOp Neg (Ident "neg") e
  Not e -> unBoolOp Not (Ident "not") e
  ERel e1 rel e2 -> binRel (`ERel` rel) (Ident (show rel)) e1 e2
  EAnd e1 e2 -> binBoolOp EAnd (Ident "(&&)") e1 e2
  EOr e1 e2 -> binBoolOp EOr (Ident "(||)") e1 e2
  EField a f ->
    case f of
      (Ident "length") -> do
        ca@(TypedExpr (ta, _)) <- inferExpr_ a
        if isArrayType ta
          then return (TypedExpr (Int, EField (typedExpr ca) f))
          else errNoArray a
      _ -> errGeneric $ "wrong field " ++ show f
  EArrInit telem (map unArrayIx -> dims) -> do
    when (isArrayType (noTemplate telem)) (errGeneric "expected non-array type")
    cdims <- mapM (checkExpr Int) dims
    let ty = iteratek (length dims) (NoTemplate . Array) telem
    return (TypedExpr (noTemplate ty, EArrInit telem (map ArrIx cdims)))
  EArrIx e (map unArrayIx -> eixs) -> do
    ce@(TypedExpr (te, _)) <- inferExpr_ e
    ixs <- map ArrIx <$> mapM (checkExpr Int) eixs
    retty <- maybe (errGeneric "too many indices") return (arrayElementType te (length eixs))
    return (TypedExpr (retty, EArrIx (typedExpr ce) ixs))
  EDeref e field -> do
    ce@(TypedExpr (te, _)) <- inferExpr_ e
    structs <- asks (_structTable . _defsTable . _methodCtx . _methodBodyCtx)
    case te of
      Pointer (NoTemplate (CustomTy structName)) -> do
        fiety <- lookupStructFieldType structs structName field
        return (TypedExpr (fiety, EDeref (typedExpr ce) field))
      _ -> errWrongType e' "The type of the expression should be a pointer to a struct"
  EMethodCall callee fun eargs -> do
    ccallee@(TypedExpr (ptrty, _)) <- inferExpr_ callee
    case ptrty of
      Pointer (NoTemplate (CustomTy className)) -> do
        classTable <- asks (_classTable . _defsTable . _methodCtx . _methodBodyCtx)
        (methodsclass, (retty, tyargs)) <- lookupClassMethod classTable className fun
        unless (length eargs == length tyargs) (errGeneric "wrong number of arguments")
        args <- zipWithM checkExpr tyargs eargs
        let supertype = Pointer (NoTemplate (CustomTy methodsclass))
        castedcallee <- castMaybe supertype ccallee
        return (TypedExpr (retty, EMethodCall (typedExpr castedcallee) fun args))
      _ -> errGeneric $ "expected a reference to an object: " ++ show e'
  ESelf -> do
    className <- asks (_class . _methodCtx . _methodBodyCtx)
      >>= maybe (errGeneric "use of self outside of a class") return
    return (type_ (Pointer (NoTemplate (CustomTy className))))
  EBlockDecl ty var (Block ss) -> withNewLevel $ do
    addVar_expr (noTemplate ty) var
    bodyctx <- asks _methodBodyCtx
    css <- withNewLevel (runReaderT (checkStmts ss) bodyctx )
    return (TypedExpr (noTemplate ty, EBlockDecl ty var (Block css)))
  EBlock var b -> geblock var b
  EAppTemplate{} -> error "template app"
  EListLit{} -> error $ "sugar node" ++ show e'
  EEnum{} -> error "sugar node"
  EListCompr{} -> error "sugar node"
  ETyExpr{} -> error "internal node"
  ETyCast{} -> error "internal node"
  where
    type_ t = TypedExpr (t, e')
    geblock var (Block ss) = do
      vl <- varLookup
      ty <- lookupVarType vl var
      bodyctx <- asks _methodBodyCtx
      css <- withNewLevel (runReaderT (checkStmts ss) bodyctx )
      return (TypedExpr (ty, EBlock var (Block css)))
    binBoolOp constructor op e1 e2 = do
      c1@(TypedExpr (t1, _)) <- inferExpr_ e1
      c2@(TypedExpr (t2, _)) <- inferExpr_ e2
      case (t1, t2) of
        (Bool, Bool) -> return (TypedExpr (Bool, constructor (typedExpr c1) (typedExpr c2)))
        _            -> errBadArgs op [[Bool, Bool]] [t1, t2]
    binRel constructor op e1 e2 = do
      c1@(TypedExpr (t1, _)) <- inferExpr_ e1
      c2@(TypedExpr (t2, _)) <- inferExpr_ e2
      let res = return (TypedExpr (Bool, constructor (typedExpr c1) (typedExpr c2)))
      case (t1, t2) of
        (Int, Int) -> res
        (Double, Double) -> res
        (Bool, Bool) -> res
        (Pointer _, Pointer _) -> res -- TODO: only for Eq and NEq
        _ -> errBadArgs op [[Double, Double], [Int, Int], [Bool, Bool]] [t1, t2]
    unBoolOp constructor op e = do
      c1@(TypedExpr (t1, _)) <- inferExpr_ e
      let res = return (TypedExpr (Bool, constructor (typedExpr c1)))
      case t1 of
        Bool -> res
        _    -> errBadArgs op [[Bool]] [t1]
    unNumOp constructor op e = do
      c1@(TypedExpr (t1, _)) <- inferExpr_ e
      let res ty = return (TypedExpr (ty, constructor (typedExpr c1)))
      case t1 of
        Int    -> res Int
        Double -> res Double
        _      -> errBadArgs op [[Double], [Int]] [t1]
    binNumOp constructor op e1 e2 = do
      c1@(TypedExpr (t1, _)) <- inferExpr_ e1
      c2@(TypedExpr (t2, _)) <- inferExpr_ e2
      let res ty = return (TypedExpr (ty, constructor (typedExpr c1) (typedExpr c2)))
      case (t1, t2) of
        (Int, Int) -> res Int
        (Double, Double)
          | isOverloaded op -> res Double
        _ -> errBadArgs (Ident $ show op) [[Double, Double], [Int, Int]] [t1, t2]


lookupClassMethod :: MonadError Error m =>
  ClassTable -> Ident -> Ident -> m (Ident, TypeSignature)
lookupClassMethod t cl meth = case Map.lookup cl t of
  Just ClassInfo{..} -> case Map.lookup meth _methods of
    Just sig -> return (cl, sig)
    _ -> case _parent of
      Just parent -> lookupClassMethod t parent meth
      _           -> errGeneric $ "Method not defined: " ++ show meth
  _ -> errGeneric $ "Class not defined: " ++ show cl

-- | Returns True when a >= b (i.e. a is equal to or is a superclass of b)
(>==) :: MonadReader ExprCtx m => Type -> Type -> m Bool
a >== b
  | a == b = return True
  | Pointer (NoTemplate (CustomTy ra)) <- a
  , Pointer (NoTemplate (CustomTy rb)) <- b = do
      mparentb <- asks ((Map.lookup rb >=> _parent) . (_classTable . _defsTable . _methodCtx . _methodBodyCtx))
      case mparentb of
        Just parentb -> a >== Pointer (NoTemplate (CustomTy parentb))
        _            -> return False
  | otherwise = return False

assignableFrom :: MonadReader ExprCtx m => Type -> Type -> m Bool
assignableFrom = (>==)

lookupFunction :: MonadError Error m => Map Ident TypeSignature -> Ident -> m TypeSignature
lookupFunction ft f =
  case Map.lookup f ft of
    Just sig -> return sig
    _        -> errFunNotDef f

-- | Tells whether an operator is overloaded or not.
isOverloaded :: Either AddOp MulOp -> Bool
isOverloaded x = case x of
  Right Mod -> False
  _         -> True
