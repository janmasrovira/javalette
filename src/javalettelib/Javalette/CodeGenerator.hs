{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE ViewPatterns        #-}
module Javalette.CodeGenerator (
  codeGen
  ) where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Bifunctor
import           Data.Generics.Uniplate.Data
import           Data.List.Extra
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe

import           Extra
import           Javalette.Common
import           Javalette.Grammar.AbsJavalette
import           Javalette.TypeChecker
import           LLVM.Gen                       as IR
import qualified LLVM.IR                        as IR


-- | The program must be already preprocessed and annotated.
codeGen :: DefsTable -> CheckedProgram -> IR.Module
codeGen defsTable@DefsTable{..} (Checked p@(Prog (noTemplateDefs -> topdefs))) = execLS $ do
  strTable <- defineStrings globalStrings
  (`runReaderT` glenv strTable) $ do
    declareExternals
    declareStructTypes
    genClassMethods
    mapM_ genTopIdentDef topdefs
  where
    declareStructTypes = sequence_ [ typedef val tys
                                    | (val, map fst -> tys) <- Map.assocs structTable ]
    structTable :: Map IR.Value [(IR.Type, String)]
    structTable = Map.fromList $
      [ (structTypeValue sname, catDecls decls)
      | StructDef sname decls <- topdefs
      ] ++
      [ (structTypeValue cl, fields)
      | (cl, clinfo) <- Map.assocs _classTable
      , let fields = consMaybe ((, "super") . structIRType <$> _parent clinfo)
              [ (mapType ty, unIdent i) | (ty, i) <- _fields clinfo ] ]
      where
        catDecls :: [DeclItem] -> [(IR.Type, String)]
        catDecls = concatMap catDecl
          where
            catDecl (DeclItem (mapType . noTemplate -> ty) idens)
              = [ (ty, i) | (Ident i) <- idens ]
    glenv :: Map String (IR.Type, IR.Value) -> GlobalEnv DefsTable
    glenv strTable = mkGEnv funTable strTable structTable defsTable
      where
        funTable = Map.union importedFuns (Map.fromList (mapMaybe funSig topdefs))
        funSig topdef =
          case topdef of
            TopFunDef (FunDef (mapType -> t) (Ident fun) (map getArgType -> tys) _) ->
              Just (fun, (t, tys))
            _ -> Nothing
        getArgType (Argument ty _) = mapType (noTemplate ty)
    declareExternals = sequence_ [
      declare retty fun args | (fun, (retty, args)) <- externalSigs]
    genClassMethods = sequence_
      [ genClassMethod cl (methodToFun md)
      | ClassDef (getClassName -> cl) items <- topdefs
      , md@ClassMethod{} <- items]
    globalStrings = collectStrings p
    importedFuns = Map.fromList [(fun, (retty, map fst args))
                                | (fun, (retty, args)) <- externalSigs]
    externalSigs = [
      ("printInt", (voidType, args [(int64Type, "n")]))
      , ("printInt_", (voidType, args [(int64Type, "n")]))
      , ("printDouble", (voidType, args [(doubleType, "d")]))
      , ("printDouble_", (voidType, args [(doubleType, "d")]))
      , ("printString", (voidType, args [(stringType, "ptr")]))
      , ("printString_", (voidType, args [(stringType, "ptr")]))
      , ("readInt", (int64Type, []))
      , ("readDouble", (doubleType, []))
      , ("mycalloc", (bytePointerType, args [(int64Type, "k"), (int64Type, "size")]))
      , ("mymalloc", (bytePointerType, args[(int64Type, "bytes")]))
      , ("myfree", (voidType, args [(bytePointerType, "ptr")]))
      ]
      where args = map (second IR.LVar)

-- | Collects all string literals in a program. String literals can only appear
-- in printString(str).
collectStrings :: Program -> [String]
collectStrings p = nubOrd [ s | EString s <- universeBi p]

genTopIdentDef :: (MonadReader (GlobalEnv DefsTable) m, MonadWriter [IR.TopDef] m)
  => IdentDef -> m ()
genTopIdentDef topdef =
  case topdef of
    TopFunDef (FunDef (mapType -> t) (Ident fun) args (Block body)) -> do
      genv <- ask
      let (rargs, blocks) = genFunArgsBody t body args genv
      define t fun rargs blocks
    _ -> return ()

genClassMethod :: (MonadReader (GlobalEnv DefsTable) m, MonadWriter [IR.TopDef] m) => Ident -> FnDef -> m ()
genClassMethod classname (FunDef (mapType -> t) (Ident fun) explicitargs (Block body)) = do
  genv <- ask
  let (rargs, blocks) = genClassMethodArgsBody classname t body allArgs genv
  define t (qualify (unIdent classname) fun) rargs blocks
  where allArgs = thisArg : explicitargs
        thisArg = selfArgument
        selfArgument = Argument (NoTemplate (Pointer (NoTemplate (CustomTy classname)))) selfIdent
genClassMethod _ _ = error "template"

selfIdent :: Ident
selfIdent = Ident "self"

qualify :: String -> String -> String
qualify a b = a ++ "." ++ b

-- | args = [this, arg1, arg2...]
genClassMethodArgsBody classname rett body args env = runCS $ (`runReaderT` env) $ do
  argptrs <- replicateM numArgs (IR.LVar <$> label)
  let thisval = head argptrs
  hassuper <- readExtra (hasSuperClass classname)
  zipWithM_ declareArg args argptrs
  bindFields hassuper thisval (IR.Declared $ structTypeValue classname)
  genStmts body
  when (rett == IR.VoidType) retVoid
  let argtypes = map (mapType . argType) args
  return $ zip argtypes argptrs
  where
    numArgs = length args

hasSuperClass :: MonadReader DefsTable m => Ident -> m Bool
hasSuperClass className = isJust . _parent <$> classInfo className

classInfo :: MonadReader DefsTable m => Ident -> m ClassInfo
classInfo className = fromMaybe (error $ "class not found " ++ show className)
  . Map.lookup className <$> asks _classTable

genFunArgsBody :: IR.Type -> [Stmt] -> [Arg] -> GlobalEnv DefsTable
  -> ([(IR.Type, IR.Value)], [IR.BasicBlock])
genFunArgsBody rett body args env = runCS $ (`runReaderT` env) $ do
  argptrs <- replicateM numArgs (IR.LVar <$> label)
  zipWithM_ declareArg args argptrs
  genStmts body
  when (rett == IR.VoidType) retVoid
  let argtypes = map (mapType . argType) args
  return $ zip argtypes argptrs
  where
    numArgs = length args

declareArg :: MonadState CS m => Arg -> IR.Value -> m ()
declareArg (Argument (mapType . noTemplate -> ty) (Ident var)) argval = do
  void $ declareVar ty var
  storeToVar var argval

initVarToZero :: (MonadReader (GlobalEnv DefsTable) m, MonadState CS m) =>
    IR.Type -> String -> m ()
initVarToZero t var
  | t == int64Type = genStmts [ass (ELitInt 0)]
  | t == doubleType = genStmts [ass (ELitDouble 0)]
  | t == boolType = genStmts [ass ELitFalse]
  | isPointer t = storeToVar var (snd (nullptr t))
  | otherwise = return ()
  where ass = Ass (LhsIdent (Ident var))

genStmts :: forall m. (MonadReader (GlobalEnv DefsTable) m, MonadState CS m) => [Stmt] -> m ()
genStmts [] = return ()
genStmts (s:ss) = case s of
  SExp e            -> genVoidExp e >> genStmts ss
  Decl ty items     -> gdecl ty items >> genStmts ss
  Ass  lhs e        -> gass lhs e >> genStmts ss
  Return e          -> gret e
  Empty             -> genStmts ss
  BStmt (Block b)   -> withScope (genStmts b) >> genStmts ss
  VReturn           -> retVoid
  Cond c t          -> gcond c t >> genStmts ss
  CondElse c t f    -> gcondelse c t f >> genStmts ss
  While c w         -> gwhile c w
  Incr (Ident var)  -> incr var >> genStmts ss
  Decr (Ident var)  -> decr var >> genStmts ss
  SIter t var arr s -> giter t var arr s >> genStmts ss
  where
    decr var = do
      (ty, val) <- loadVar var
      r <- sub val (one ty)
      storeToVar var r
    incr var = do
      (ty, val) <- loadVar var
      r <- add (one ty) val
      storeToVar var r
    declareInitItem (mapType . noTemplate -> t) i = do
      case i of
        NoInit (Ident var) -> do
          void $ declareVar t var
          initVarToZero t var
        Init (Ident var) e -> do
          vexp <- genExp' e
          void $ declareVar t var
          storeToVar var vexp
    gdecl ty = mapM_ (declareInitItem ty)
    gret = genExp >=> ret
    gass lhs e =
      case lhs of
        LhsIdent (Ident var) -> genExp' e >>= storeToVar var
        LhsArray (Ident var) (map unArrayIx -> eixs) -> do
          ixs <- mapM genExp eixs
          genExp' e >>= storeArrayVarElem var ixs
        LhsPtr (Ident ptrvar) (Ident field) ->
          genExp e >>= storeToVarField ptrvar field
    gcondelse c t f
     | Just False <- sc = withScope (genStmts [f])
     | Just True <- sc = withScope (genStmts [t])
     | otherwise = do
         rc <- genExp' c
         ltrue <- label' "iftrue"
         lfalse <- label' "iffalse"
         lend <- label' "endif"
         brCond rc ltrue lfalse
         startBlock ltrue
         withScope (genStmts [t])
         unless rets (br lend)
         startBlock lfalse
         withScope (genStmts [f])
         unless rets (br lend >> startBlock lend)
      where sc = staticEvalBool c
            rets = reachRet (CondElse c t f)
    gcond c s
      | Just False <- sc = return ()
      | Just True <- sc = withScope (genStmts [s])
      | otherwise = do
          rc <- genExp' c
          ltrue <- label' "iftrue"
          lend <- label' "endif"
          brCond rc ltrue lend
          startBlock ltrue
          withScope $ genStmts [s]
          unless rets (br lend >> startBlock lend)
            where sc = staticEvalBool c
                  rets = reachRet (Cond c s)
    gwhile c s
      | Just False <- sc = genStmts ss
      | Just True <- sc = do
          lbody <- label' "while"
          br lbody
          startBlock lbody
          withScope (genStmts [s])
          unless rets (br lbody)
      | otherwise = do
          lcond <- label' "cond"
          lbody <- label' "body"
          lend <- label' "endwhile"
          br lcond

          startBlock lcond
          rc <- genExp' c
          brCond rc lbody lend

          startBlock lbody
          withScope (genStmts [s])
          br lcond

          unless rets (startBlock lend >> genStmts ss)
      where sc = staticEvalBool c
            rets = reachRet (While c s)
    -- for (t v : a) s ===
    -- int ix = 0;
    -- int len = a.length
    -- while (ix < a.length) {v = a[ix]; s; ix++;}
    giter tvar itVar earr body = do
      arrVar <- Ident <$> internalVar "arr" -- needed to avoid calculating multiple types earr.
      ixVar <- Ident <$> internalVar "ix"
      lenVar <- Ident <$> internalVar "len"  -- not strictly needed by I prefer to have it.

      genStmts [Decl (NoTemplate (Array tvar)) [Init arrVar earr]]
      genStmts [Decl (NoTemplate Int) [Init lenVar (EField (EVar arrVar) (Ident "length")), NoInit ixVar]] -- ix initialized to 0 automatically.

      let cond = ERel (EVar ixVar) LTH (EVar lenVar)
          itbody = BStmt $ Block [
            Decl tvar [Init itVar(EArrIx (EVar arrVar) [ArrIx (EVar ixVar)])] -- v = a[ix]
            , body
            , Incr ixVar -- ++ix;
            ]
      genStmts [While cond itbody]

mapType :: Type -> IR.Type
mapType ty = case ty of
  Int                          -> int64Type
  Double                       -> doubleType
  Void                         -> voidType
  Bool                         -> boolType
  String                       -> stringType
  Array (mapType . noTemplate -> r) -> arrayOfType r
  Pointer (mapType . noTemplate -> t) -> IR.Pointer t
  CustomTy ct                    -> IR.Declared (structTypeValue ct)

stuctTypeName :: Ident -> Ident
stuctTypeName (Ident i) = Ident i

structTypeValue :: Ident -> IR.Value
structTypeValue = IR.LVar . unIdent . stuctTypeName

structIRType :: Ident -> IR.Type
structIRType = IR.Declared . structTypeValue

genVoidExp :: forall m. (MonadReader (GlobalEnv DefsTable) m, MonadState CS m)
  => Expr -> m ()
genVoidExp e = case e of
  ETyExpr Void a        -> genVoidExp a
  EFree e               -> genExp e >>= freePtr
  EApp (Ident fun) args -> do
    retty <- lookupRetType fun
    if retty == voidType
      then gvoidapp fun args
      else void (genExp e)
  EMethodCall e fun args -> genMethodCall (const callVoid) e fun args
  _ -> void (genExp e)
  where
    gvoidapp fun args = do
      rargs <- mapM genExp args
      callVoid fun rargs

fromPointer :: IR.Type -> IR.Type
fromPointer (IR.Pointer t) = t
fromPointer _ = error "unexpected type"

genMethodCall :: (MonadReader (GlobalEnv DefsTable) m, MonadState CS m) =>
     (IR.Type -> String -> [(IR.Type, IR.Value)] -> m b)
     -> Expr -> Ident -> [Expr] -> m b
genMethodCall callfun eobj fun eargs = do
      (ty, ptr) <- first fromPointer <$> genExp eobj
      explicitargs <- mapM genExp eargs
      classtable <- asksExtra _classTable
      let callingclass = classNameFromIRType ty
          (methodsclass, mapType . fst -> retty) =
            unsafe $ lookupClassMethod classtable callingclass fun
          genallargs
            | methodsclass == callingclass = return $ (IR.Pointer ty, ptr) : explicitargs
            | otherwise = error "missing cast annotation"
      allargs <- genallargs
      callfun retty (qualify (unIdent methodsclass) (unIdent fun)) allargs

genExp' :: (MonadReader (GlobalEnv DefsTable) m, MonadState CS m) => Expr -> m IR.Value
genExp' e = snd <$> genExp e

genExp :: forall m. (MonadReader (GlobalEnv DefsTable) m, MonadState CS m) =>
  Expr -> m (IR.Type, IR.Value)
genExp e = case e of
  ELitInt int             -> return (int64Type, IR.IntLit (fromInteger int))
  ELitDouble d            -> return (doubleType, IR.DoubleLit d)
  EAdd a op b             -> gadd a op b
  EApp (Ident fun) args   -> gapp fun args
  EVar (Ident var)        -> loadVar var
  ELitTrue                -> return (boolType, IR.IntLit 1)
  ELitFalse               -> return (boolType, IR.IntLit 0)
  Neg a                   -> gneg a
  Not a                   -> gnot a
  ERel a rel b            -> grel a rel b
  EMul a op b             -> gmul a op b
  EAnd a b                -> glorand True a b
  EOr a b                 -> glorand False a b
  EString str             -> getStringPtr str
  EArrInit t ixs          -> garrini t (map unArrayIx ixs)
  EField a (Ident f)      -> gfield a f
  EArrIx a ixs            -> garrix a ixs
  EDeref e (Ident f)      -> gderef e f
  ENew ptrty              -> gnew ptrty
  ENullPtr (noTemplate -> ty)             -> gnullptr ty
  EMethodCall e m as      -> genMethodCall call e m as
  EBlockDecl ty (Ident var) b -> geblockdecl ty var b
  EBlock (Ident var) b    -> geblock var b
  ETyExpr ty ESelf        -> loadVar (unIdent selfIdent)
  ETyExpr ty a            -> genExp a
  ETyCast t0 e t1         -> gcast t0 e t1
  EAppTemplate{}          -> error "template app"
  EFree{}                 -> error "EFree is void"
  ESelf                   -> error "Invalid use of self"
  EListCompr{}            -> error "sugar expr"
  EListLit{}              -> error "sugar expr"
  EEnum{}                 -> error "sugar expr"
  where
    geblock var (Block ss) = do
      withScope (genStmts ss)
      loadVar var
    geblockdecl (mapType . noTemplate -> ty) var (Block ss) = withScope $ do
      void $ declareVar ty var
      initVarToZero ty var
      withScope (genStmts ss)
      loadVar var
    gcast t0 a t1 = do
      ga <- genExp a
      bitcast ga (mapType t1)
    gfield a f = case f of
      "length" -> genExp a >>= loadArrayLen
      _        -> error "unrecognized field"
    gnullptr jt = return $ nullptr (mapType jt)
    gnew (mapType . noTemplate -> ty) =
      case ty of
        IR.Declared tyref -> allocateStruct tyref
        _                 -> error $ "enew wrong type: " ++ show ty
    gderef e f = do
      struct <- genExp e
      loadField struct f
    grel a rel b = do
      (ty, ra) <- genExp a
      rb <- genExp' b
      let icmp' r = icmp r ty ra rb
          fcmp' r = fcmp r ty ra rb
          irrel = mapRelOp ty
      either icmp' fcmp' irrel
        where
          mapRelOp :: IR.Type -> Either IR.IComparison IR.FComparison
          mapRelOp ty = case rel of
            LTH -> sel (IR.SLT, IR.OLT)
            LE  -> sel (IR.SLE, IR.OLE)
            GTH -> sel (IR.SGT, IR.OGT)
            GE  -> sel (IR.SGE, IR.OGE)
            EQU -> sel (IR.Eq, IR.OEq)
            NE  -> sel (IR.NotEq, IR.ONE)
            where sel
                    | IR.IntegerType _ <- ty = Left . fst
                    | IR.DoubleType <- ty = Right . snd
                    | IR.Pointer _ <- ty = Left . fst
                    | otherwise = error $ "cannot compare things of type " ++ show ty
    gneg a = do
      (ty, re) <- genExp a
      let z = zero ty
      if
        | int64Type == ty -> (ty, ) <$> sub z re
        | doubleType == ty -> (ty, ) <$> fsub z re
        | otherwise -> error "neg"
    zero ty =  numlit ty 0
    gnot a = do
      ra <- genExp' a
      icmp IR.Eq boolType ra (IR.IntLit 0)
    gadd a op b = do
      (ty, ra) <- genExp a
      rb <- genExp' b
      case op of
        Plus
          | ty == int64Type -> (int64Type, ) <$> add ra rb
          | ty == doubleType -> (doubleType, ) <$> fadd ra rb
          | otherwise -> error "type error"
        Minus
          | ty == int64Type -> (int64Type, ) <$> sub ra rb
          | ty == doubleType -> (doubleType, ) <$> fsub ra rb
          | otherwise -> error "minus"
    gmul a op b = do
      (ty, ra) <- genExp a
      rb <- genExp' b
      case op of
        Times
          | ty == int64Type -> (int64Type, ) <$> mul ra rb
          | ty == doubleType -> (doubleType, ) <$> fmul ra rb
          | otherwise -> error "times"
        Div
          | ty == int64Type -> (int64Type, ) <$> sdiv ra rb
          | ty == doubleType -> (doubleType, ) <$> fdiv ra rb
          | otherwise -> error "div"
        Mod
          | ty == int64Type -> (int64Type, ) <$> srem ra rb
          | otherwise -> error "mod"
    glorand isAnd a b = do
      lop2 <- label' "op2"
      lend <- label' "endAndOr"
      ra <- genExp' a
      lop1 <- getCurrentLabel
      if isAnd
        then brCond ra lop2 lend
        else brCond ra lend lop2
      startBlock lop2
      rb <- genExp' b
      lop2end <- getCurrentLabel
      br lend
      startBlock lend
      phi boolType [(ra, lop1), (rb, lop2end)]
    gapp :: String -> [Expr] -> m (IR.Type, IR.Value)
    gapp fun args = do
      rargs <- mapM genExp args
      retty <- lookupRetType fun
      if
        | retty == voidType -> error "void expression"
        | otherwise         -> call retty fun rargs
    -- | Returns a pointer to an allocated array in the heap
    garrini (mapType . noTemplate -> basetype) edims = do
      dims <- mapM genExp edims
      allocateArray (arrayOfBaseType basetype (length dims)) dims
    garrix a (map unArrayIx -> eixs) = do
      ixs <- mapM genExp eixs
      ra <- genExp a
      loadArrayElem ra ixs

classNameFromIRType :: IR.Type -> Ident
classNameFromIRType ty = case ty of
  IR.Declared (IR.LVar s) -> Ident s
  _                       -> error "wrong type"


one :: IR.Type -> IR.Value
one ty = numlit ty 1

numlit :: IR.Type -> Int -> IR.Value
numlit ty n = case ty of
  IR.DoubleType    -> IR.DoubleLit $ fromIntegral n
  IR.IntegerType _ -> IR.IntLit n
  _                -> error "not a numeric type"
