module Javalette.Builtin.List (
  builtinListsTransform
  ) where

import           Data.Generics.Uniplate.Data
import           Javalette.Grammar.AbsJavalette

-- | This transformation should be applied before the preprocess.
builtinListsTransform :: Program -> Program
builtinListsTransform p0 = Prog ([templateNodeClassDef, templateListClassDef, templateStaticEnumDef] ++ ds1)
  where
    (Prog ds1) = desugarLists p0

-- | Apply before substituting types.
desugarLists :: Program -> Program
desugarLists = transformBi subs
  where
    subs :: Expr -> Expr
    subs (EListLit tyelem elems) =
      EBlockDecl (listType tyelem) varId $ Block $
      Ass (LhsIdent varId) (ENew (listType tyelem))
      : [SExp $ EMethodCall (EVar varId) consIdent [ele] | ele <- reverse elems]
      -- foldr staticCons (ENew (NoTemplate (List (NoTemplate tyelem)))) elems
      where
        varId = Ident "lst.lit"
    subs (EEnum ini end) = EApp staticEnumIdent [ini, end]
    subs (EListCompr tyelem expr lss) =
      EBlockDecl (listType tyelem) listVarIdent (
      Block (Ass (LhsIdent listVarIdent) (ENew (listType tyelem))
             : desugarLStmts lss))
        where
          desugarLStmts :: [ListComprStmt] -> [Stmt]
          desugarLStmts (s:ss) = case s of
            LGenerator tyvar var lst -> [
              Decl (listType tyvar) [Init itVarIdent (EMethodCall lst iteratorIdent [])]
              , While (Not (EMethodCall (EVar itVarIdent) emptyIdent []))
                (BStmt $ Block $
                    Decl tyvar [Init var (EMethodCall (EVar itVarIdent) popIdent [])]
                    : desugarLStmts ss)
              ]
            LDecl ty var val -> [
              BStmt $ Block (
                  Decl ty [Init var val]
                  : desugarLStmts ss
                  )]
            LGuard cond -> [
              Cond cond (BStmt (Block (desugarLStmts ss)))
              ]
          desugarLStmts [] =
            [SExp (EMethodCall (EVar listVarIdent) appendIdent [expr])]
          listVarIdent = Ident "internal.listcomp"
          itVarIdent = Ident "internal.it"
    subs e = e

listType :: TypeTemplate -> TypeTemplate
listType tyelem = Template listIdent [tyelem] []

listIdent :: Ident
listIdent = Ident "List"

nodeIdent :: Ident
nodeIdent = Ident "Node"

emptyIdent :: Ident
emptyIdent = Ident "empty"

iteratorIdent :: Ident
iteratorIdent = Ident "iterator"

popIdent :: Ident
popIdent = Ident "pop"

consIdent :: Ident
consIdent = Ident "cons"

appendIdent :: Ident
appendIdent = Ident "append"

staticEnumIdent :: Ident
staticEnumIdent = Ident "jlist.enum"

templateNodeClassDef :: TopDef
templateNodeClassDef =
  TemplateDef
    [Ident "T"]
    (ClassDef
       (NoParent nodeIdent)
       [ ClassField (CustomTy (Ident "T")) [Ident "elem"]
       , ClassFieldTemplate
           nodeIdent
           [NoTemplate (CustomTy (Ident "T"))] []
           [Ident "next"]
       , ClassMethod
           (FunDefTemplate
              nodeIdent
              [NoTemplate (CustomTy (Ident "T"))] []
              (Ident "getNext")
              []
              (Block [Return (EVar (Ident "next"))]))
       , ClassMethod
           (FunDef
              Void
              (Ident "setNext")
              [ Argument
                  (Template nodeIdent [NoTemplate (CustomTy (Ident "T"))] [])
                  (Ident "l")
              ]
              (Block [Ass (LhsIdent (Ident "next")) (EVar (Ident "l"))]))
       , ClassMethod
           (FunDef
              (CustomTy (Ident "T"))
              (Ident "getElem")
              []
              (Block [Return (EVar (Ident "elem"))]))
       , ClassMethod
           (FunDef
              Void
              (Ident "setElem")
              [Argument (NoTemplate (CustomTy (Ident "T"))) (Ident "nodexset")]
              (Block [Ass (LhsIdent (Ident "elem")) (EVar (Ident "nodexset"))]))
       ])

templateStaticEnumDef :: TopDef
templateStaticEnumDef =
  NoTemplateDef
    (TopFunDef
       (FunDefTemplate
          listIdent
          [NoTemplate Int] []
          staticEnumIdent
          [ Argument (NoTemplate Int) (Ident "a")
          , Argument (NoTemplate Int) (Ident "b")
          ]
          (Block
             [ Decl
                 (Template listIdent [NoTemplate Int] [])
                 [ Init
                     (Ident "l")
                     (ENew (Template listIdent [NoTemplate Int] []))
                 ]
             , While
                 (ERel (EVar (Ident "a")) LE (EVar (Ident "b")))
                 (BStmt
                    (Block
                       [ SExp
                           (EMethodCall
                              (EVar (Ident "l"))
                              (Ident "append")
                              [EVar (Ident "a")])
                       , Incr (Ident "a")
                       ]))
             , Return (EVar (Ident "l"))
             ])))


templateListClassDef :: TopDef
templateListClassDef =
  TemplateDef
    [Ident "T"]
    (ClassDef
       (NoParent listIdent)
       [ ClassFieldTemplate
           (Ident "Node")
           [NoTemplate (CustomTy (Ident "T"))]
           []
           [Ident "first"]
       , ClassMethod
           (FunDef
              Void
              (Ident "vpop")
              []
              (Block
                 [ Ass
                     (LhsIdent (Ident "first"))
                     (EMethodCall (EVar (Ident "first")) (Ident "getNext") [])
                 ]))
       , ClassMethod
           (FunDefTemplate
              (Ident "Node")
              [NoTemplate (CustomTy (Ident "T"))]
              []
              (Ident "getPtr")
              []
              (Block [Return (EVar (Ident "first"))]))
       , ClassMethod
           (FunDefTemplate
              (Ident "Node")
              [NoTemplate (CustomTy (Ident "T"))]
              []
              (Ident "ptrAt")
              [Argument (NoTemplate Int) (Ident "ix")]
              (Block
                 [ Decl
                     (Template
                        (Ident "Node")
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [Init (Ident "r") (EVar (Ident "first"))]
                 , While
                     (ERel (EVar (Ident "ix")) GTH (ELitInt 0))
                     (BStmt
                        (Block
                           [ Decr (Ident "ix")
                           , Ass
                               (LhsIdent (Ident "r"))
                               (EMethodCall
                                  (EVar (Ident "r"))
                                  (Ident "getNext")
                                  [])
                           ]))
                 , Return (EVar (Ident "r"))
                 ]))
       , ClassMethod
           (FunDef
              Void
              (Ident "concat")
              [ Argument
                  (Template
                     listIdent
                     [NoTemplate (CustomTy (Ident "T"))]
                     [])
                  (Ident "l")
              ]
              (Block
                 [ Cond
                     (EMethodCall ESelf (Ident "empty") [])
                     (BStmt
                        (Block
                           [ Ass
                               (LhsIdent (Ident "first"))
                               (EMethodCall
                                  (EVar (Ident "l"))
                                  (Ident "getPtr")
                                  [])
                           , VReturn
                           ]))
                 , Decl
                     (Template
                        (Ident "Node")
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "last")
                         (EMethodCall
                            ESelf
                            (Ident "ptrAt")
                            [ EAdd
                                (EMethodCall ESelf (Ident "length") [])
                                Minus
                                (ELitInt 1)
                            ])
                     ]
                 , SExp
                     (EMethodCall
                        (EVar (Ident "last"))
                        (Ident "setNext")
                        [EMethodCall (EVar (Ident "l")) (Ident "getPtr") []])
                 ]))
       , ClassMethod
           (FunDef
              Void
              (Ident "set")
              [ Argument (NoTemplate Int) (Ident "ix")
              , Argument (NoTemplate (CustomTy (Ident "T"))) (Ident "x")
              ]
              (Block
                 [ Decl
                     (Template
                        (Ident "Node")
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "p")
                         (EMethodCall ESelf (Ident "ptrAt") [EVar (Ident "ix")])
                     ]
                 , SExp
                     (EMethodCall
                        (EVar (Ident "p"))
                        (Ident "setElem")
                        [EVar (Ident "x")])
                 ]))
       , ClassMethod
           (FunDef
              (CustomTy (Ident "T"))
              (Ident "at")
              [Argument (NoTemplate Int) (Ident "ix")]
              (Block
                 [ Decl
                     (Template
                        (Ident "Node")
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "p")
                         (EMethodCall ESelf (Ident "ptrAt") [EVar (Ident "ix")])
                     ]
                 , Return (EMethodCall (EVar (Ident "p")) (Ident "getElem") [])
                 ]))
       , ClassMethod
           (FunDef
              Int
              (Ident "length")
              []
              (Block
                 [ Decl
                     (Template
                        (Ident "Node")
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [Init (Ident "tmp") (EVar (Ident "first"))]
                 , Decl (NoTemplate Int) [Init (Ident "l") (ELitInt 0)]
                 , While
                     (ERel
                        (EVar (Ident "tmp"))
                        NE
                        (ENullPtr
                           (Template
                              (Ident "Node")
                              [NoTemplate (CustomTy (Ident "T"))]
                              [])))
                     (BStmt
                        (Block
                           [ Incr (Ident "l")
                           , Ass
                               (LhsIdent (Ident "tmp"))
                               (EMethodCall
                                  (EVar (Ident "tmp"))
                                  (Ident "getNext")
                                  [])
                           ]))
                 , Return (EVar (Ident "l"))
                 ]))
       , ClassMethod
           (FunDef
              Bool
              (Ident "empty")
              []
              (Block
                 [ Return
                     (ERel
                        (ELitInt 0)
                        EQU
                        (EMethodCall ESelf (Ident "length") []))
                 ]))
       , ClassMethod
           (FunDef
              (CustomTy (Ident "T"))
              (Ident "remove")
              [Argument (NoTemplate Int) (Ident "ix")]
              (Block
                 [ Decl (NoTemplate (CustomTy (Ident "T"))) [NoInit (Ident "r")]
                 , CondElse
                     (ERel (EVar (Ident "ix")) EQU (ELitInt 0))
                     (BStmt
                        (Block
                           [ Ass
                               (LhsIdent (Ident "r"))
                               (EMethodCall
                                  (EVar (Ident "first"))
                                  (Ident "getElem")
                                  [])
                           , Ass
                               (LhsIdent (Ident "first"))
                               (EMethodCall
                                  (EVar (Ident "first"))
                                  (Ident "getNext")
                                  [])
                           ]))
                     (BStmt
                        (Block
                           [ Decl
                               (Template
                                  (Ident "Node")
                                  [NoTemplate (CustomTy (Ident "T"))]
                                  [])
                               [ Init
                                   (Ident "prev")
                                   (EMethodCall
                                      ESelf
                                      (Ident "ptrAt")
                                      [ EAdd
                                          (EVar (Ident "ix"))
                                          Minus
                                          (ELitInt 1)
                                      ])
                               ]
                           , Ass
                               (LhsIdent (Ident "r"))
                               (EMethodCall
                                  (EMethodCall
                                     (EVar (Ident "prev"))
                                     (Ident "getNext")
                                     [])
                                  (Ident "getElem")
                                  [])
                           , SExp
                               (EMethodCall
                                  (EVar (Ident "prev"))
                                  (Ident "setNext")
                                  [ EMethodCall
                                      (EMethodCall
                                         (EVar (Ident "prev"))
                                         (Ident "getNext")
                                         [])
                                      (Ident "getNext")
                                      []
                                  ])
                           ]))
                 , Return (EVar (Ident "r"))
                 ]))
       , ClassMethod
           (FunDef
              Void
              (Ident "insert")
              [ Argument (NoTemplate Int) (Ident "ix")
              , Argument (NoTemplate (CustomTy (Ident "T"))) (Ident "x")
              ]
              (Block
                 [ Decl
                     (Template
                        (Ident "Node")
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "xnode")
                         (ENew
                            (Template
                               (Ident "Node")
                               [NoTemplate (CustomTy (Ident "T"))]
                               []))
                     ]
                 , SExp
                     (EMethodCall
                        (EVar (Ident "xnode"))
                        (Ident "setElem")
                        [EVar (Ident "x")])
                 , CondElse
                     (ERel (EVar (Ident "ix")) EQU (ELitInt 0))
                     (BStmt
                        (Block
                           [ SExp
                               (EMethodCall
                                  (EVar (Ident "xnode"))
                                  (Ident "setNext")
                                  [EVar (Ident "first")])
                           , Ass
                               (LhsIdent (Ident "first"))
                               (EVar (Ident "xnode"))
                           ]))
                     (BStmt
                        (Block
                           [ Decl
                               (Template
                                  (Ident "Node")
                                  [NoTemplate (CustomTy (Ident "T"))]
                                  [])
                               [ Init
                                   (Ident "p")
                                   (EMethodCall
                                      ESelf
                                      (Ident "ptrAt")
                                      [ EAdd
                                          (EVar (Ident "ix"))
                                          Minus
                                          (ELitInt 1)
                                      ])
                               ]
                           , SExp
                               (EMethodCall
                                  (EVar (Ident "xnode"))
                                  (Ident "setNext")
                                  [ EMethodCall
                                      (EVar (Ident "p"))
                                      (Ident "getNext")
                                      []
                                  ])
                           , SExp
                               (EMethodCall
                                  (EVar (Ident "p"))
                                  (Ident "setNext")
                                  [EVar (Ident "xnode")])
                           ]))
                 ]))
       , ClassMethod
           (FunDef
              Void
              (Ident "cons")
              [Argument (NoTemplate (CustomTy (Ident "T"))) (Ident "x")]
              (Block
                 [ SExp
                     (EMethodCall
                        ESelf
                        (Ident "insert")
                        [ELitInt 0, EVar (Ident "x")])
                 ]))
       , ClassMethod
           (FunDef
              (CustomTy (Ident "T"))
              (Ident "pop")
              []
              (Block [Return (EMethodCall ESelf (Ident "remove") [ELitInt 0])]))
       , ClassMethod
           (FunDef
              Void
              (Ident "append")
              [Argument (NoTemplate (CustomTy (Ident "T"))) (Ident "d")]
              (Block
                 [ SExp
                     (EMethodCall
                        ESelf
                        (Ident "insert")
                        [ EMethodCall ESelf (Ident "length") []
                        , EVar (Ident "d")
                        ])
                 ]))
       , ClassMethod
           (FunDef
              (CustomTy (Ident "T"))
              (Ident "head")
              []
              (Block
                 [ Return
                     (EMethodCall (EVar (Ident "first")) (Ident "getElem") [])
                 ]))
       , ClassMethod
           (FunDefTemplate
              listIdent
              [NoTemplate (CustomTy (Ident "T"))]
              []
              (Ident "shallowCopy")
              []
              (Block
                 [ Decl
                     (Template
                        listIdent
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "c")
                         (ENew
                            (Template
                               listIdent
                               [NoTemplate (CustomTy (Ident "T"))]
                               []))
                     ]
                 , Decl
                     (NoTemplate (Array (NoTemplate (CustomTy (Ident "T")))))
                     [Init (Ident "a") (EMethodCall ESelf (Ident "toArray") [])]
                 , Decl
                     (NoTemplate Int)
                     [ Init
                         (Ident "i")
                         (EAdd
                            (EField (EVar (Ident "a")) (Ident "length"))
                            Minus
                            (ELitInt 1))
                     ]
                 , While
                     (ERel (EVar (Ident "i")) GE (ELitInt 0))
                     (BStmt
                        (Block
                           [ SExp
                               (EMethodCall
                                  (EVar (Ident "c"))
                                  (Ident "cons")
                                  [ EArrIx
                                      (EVar (Ident "a"))
                                      [ArrIx (EVar (Ident "i"))]
                                  ])
                           , Decr (Ident "i")
                           ]))
                 , Return (EVar (Ident "c"))
                 ]))
       , ClassMethod
           (FunDef
              (Array (NoTemplate (CustomTy (Ident "T"))))
              (Ident "toArray")
              []
              (Block
                 [ Decl
                     (Template
                        listIdent
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "it")
                         (EMethodCall ESelf (Ident "iterator") [])
                     ]
                 , Decl
                     (NoTemplate (Array (NoTemplate (CustomTy (Ident "T")))))
                     [ Init
                         (Ident "a")
                         (EArrInit
                            (NoTemplate (CustomTy (Ident "T")))
                            [ArrIx (EMethodCall ESelf (Ident "length") [])])
                     ]
                 , Decl (NoTemplate Int) [Init (Ident "i") (ELitInt 0)]
                 , While
                     (ERel
                        (EVar (Ident "i"))
                        LTH
                        (EField (EVar (Ident "a")) (Ident "length")))
                     (BStmt
                        (Block
                           [ Ass
                               (LhsArray (Ident "a") [ArrIx (EVar (Ident "i"))])
                               (EMethodCall (EVar (Ident "it")) (Ident "pop") [])
                           , Incr (Ident "i")
                           ]))
                 , Return (EVar (Ident "a"))
                 ]))
       , ClassMethod
           (FunDefTemplate
              listIdent
              [NoTemplate (CustomTy (Ident "T"))]
              []
              (Ident "iterator")
              []
              (Block
                 [ Decl
                     (Template
                        listIdent
                        [NoTemplate (CustomTy (Ident "T"))]
                        [])
                     [ Init
                         (Ident "l")
                         (ENew
                            (Template
                               listIdent
                               [NoTemplate (CustomTy (Ident "T"))]
                               []))
                     ]
                 , SExp
                     (EMethodCall
                        (EVar (Ident "l"))
                        (Ident "setPtr")
                        [EMethodCall ESelf (Ident "getPtr") []])
                 , Return (EVar (Ident "l"))
                 ]))
       , ClassMethod
           (FunDef
              Void
              (Ident "setPtr")
              [ Argument
                  (Template
                     (Ident "Node")
                     [NoTemplate (CustomTy (Ident "T"))]
                     [])
                  (Ident "ptr")
              ]
              (Block [Ass (LhsIdent (Ident "first")) (EVar (Ident "ptr"))]))
       ])
