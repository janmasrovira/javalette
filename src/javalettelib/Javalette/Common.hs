{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE ViewPatterns        #-}
module Javalette.Common where

import           Control.Monad.Except
import           Data.Generics.Uniplate.Data
import           Data.List
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe
import           Data.Set                       (Set)
import qualified Data.Set                       as Set
import           Extra
import           Javalette.Error
import           Javalette.Grammar.AbsJavalette

type TypeSignature = (Type, [Type])
type ClassTable = Map Ident ClassInfo
type StructTable = Map Ident (Map Ident Type)
type TypedefTable = Map Ident Type
type FunTable = Map Ident TypeSignature


data ClassInfo = ClassInfo {
  _parent    :: Maybe Ident
  , _fields  :: [(Type, Ident)]
  , _methods :: FunTable
  }


data DefsTable = DefsTable {
  _funTable      :: FunTable
  , _structTable :: StructTable
  , _classTable  :: ClassTable
  }


unIdent :: Ident -> String
unIdent (Ident i) = i

isArrayType :: Type -> Bool
isArrayType = isJust . unArrayType

unArrayIx :: ArrayIx -> Expr
unArrayIx (ArrIx e) = e

unArrayType :: Type -> Maybe Type
unArrayType (Array (noTemplate -> t)) = Just t
unArrayType _                         = Nothing

argType :: Arg -> Type
argType (Argument (noTemplate -> t) _) = t

catDeclItem :: DeclItem -> [(Type, Ident)]
catDeclItem (DeclItem (noTemplate -> ty) idens) = map (ty, ) idens

funSignature :: FnDef -> (Ident, TypeSignature)
funSignature (FunDef ret name args _) = (name, (ret, map argType args))
funSignature _                        = error "template FnDef"

typeSignature :: FnDef -> TypeSignature
typeSignature = snd . funSignature

isPointerType :: Type -> Bool
isPointerType = isJust . unPointerType

unPointerType :: Type -> Maybe TypeTemplate
unPointerType (Pointer t) = Just t
unPointerType _           = Nothing

unTypeTemplate :: TypeTemplate -> (Type, Maybe [TypeTemplate])
unTypeTemplate (Template n ts (length -> dims)) = (iteratek dims (Array . NoTemplate) (CustomTy n), Just ts)
unTypeTemplate (NoTemplate t)  = (t, Nothing)

-- | Supposed to be used after template expansion.
noTemplate :: TypeTemplate -> Type
noTemplate t
  | (ty, Nothing) <- unTypeTemplate t = ty
  | otherwise = error "template found"

noTemplateDef :: TopDef -> IdentDef
noTemplateDef (NoTemplateDef d) = d
noTemplateDef t@TemplateDef{}   = error $ "noTemplateDef " ++ show t

noTemplateDefs :: [TopDef] -> [IdentDef]
noTemplateDefs = map noTemplateDef

methodToFun :: ClassItem -> FnDef
methodToFun (ClassMethod f) = f
methodToFun d               = error $ "def is not a method" ++ show d

funToMethod :: FnDef -> ClassItem
funToMethod = ClassMethod

typeToIdent :: TypeTemplate -> Ident
typeToIdent = Ident . typeToIdentStr

arrayToks :: Ident -> [ArrayToken] -> Type
arrayToks i (length -> dims) = iteratek dims (Array . NoTemplate) (CustomTy i)

typeToIdentStr :: TypeTemplate -> String
typeToIdentStr tt = case tt of
  Template (Ident i) ts (length -> dims) ->
    iteratek dims ("array." ++) $
    i ++ llvmparens (llvmCommaSep (map typeToIdentStr ts))
  NoTemplate ty -> case ty of
    Int                -> "int"
    Double             -> "double"
    CustomTy (Ident i) -> i
    Bool               -> "boolean"
    Array t            -> "array." ++ typeToIdentStr t
    Pointer t          -> llvmparens (typeToIdentStr t) ++ "*";
    _                  -> error "typetoident"
  where llvmparens str = "$." ++ str ++ ".$"
        llvmCommaSep = intercalate ".."

getClassName :: ClassName -> Ident
getClassName (Parent a _) = a
getClassName (NoParent a) = a

buildClassTable :: forall m. MonadError Error m => Program -> m ClassTable
buildClassTable (Prog (noTemplateDefs -> ds)) = foldM addClass mempty ds
  where
    addClass tbl (ClassDef namep citems) =
      case insertLookup name clinfo tbl of
        (Nothing, t') -> return t'
        _             -> errDupClass
      where
        clinfo = ClassInfo {..}
        _fields = concatMap catDeclItem [ DeclItem (NoTemplate ty) names | ClassField ty names <- citems ]
        _methods = Map.fromList [ funSignature d | ClassMethod d <- citems ]
        errDupClass = errGeneric $ "Duplicate class definition " ++ show name
        (name, _parent) = case namep of
          Parent a b -> (a, Just b)
          NoParent a -> (a, Nothing)
    addClass tbl _ = return tbl

substAliasProg :: MonadError Error m => Program -> m Program
substAliasProg p = (`substAlias`p) <$> buildTypedefsTable p

-- | Substitutes typedef aliases with their real type.
substAlias :: TypedefTable -> Program -> Program
substAlias tbl = transformBi got
  where
    got ty@(CustomTy iden) = fromMaybe ty $ Map.lookup iden tbl
    got ty                 = ty


buildTypedefsTable :: MonadError Error m => Program -> m (Map Ident Type)
buildTypedefsTable (Prog (noTemplateDefs -> ds)) = foldM addTypedef mempty ds
  where
    addTypedef tbl def =
      case def of
        TypeDef realty alias -> do
          when (alias == realty) (errGeneric "typedef loop")
          case insertLookup alias (Pointer (NoTemplate (CustomTy realty))) tbl of
            (Nothing, t') -> return t'
            _             -> errGeneric "duplicated alias definition"
        _ -> return tbl

subsObjTypesToPointersProg :: MonadError Error f => Program -> f Program
subsObjTypesToPointersProg p = (`substObjTypesToPointers`p) <$> buildClassTable p

substObjTypesToPointers :: ClassTable -> Program -> Program
substObjTypesToPointers tbl = transformBi unmarknew . rewriteBi unmarkty . rewriteBi markty . transformBi marknews
  where
    marknews e
      | ENew (NoTemplate (CustomTy (Ident i))) <- e =
          ENew (NoTemplate (CustomTy (Ident (newmark ++ i))))
      | otherwise = e
    unmarknew ty
      | CustomTy (Ident i) <- ty, newmark == take (length newmark) i =
          CustomTy (Ident (drop (length newmark) i))
      | otherwise = ty
    markty ty
      | CustomTy iden <- ty, Map.member iden tbl = Just (CustomTy (putmark iden))
      | otherwise = Nothing
    xxx = ":mark:"
    newmark = ":new:"
    putmark (Ident i) = Ident (xxx ++ i)
    unmarkty ty
      | CustomTy (Ident i) <- ty, xxx == take (length xxx) i =
          Just (Pointer (NoTemplate (CustomTy (Ident (drop (length xxx) i)))))
      | otherwise = Nothing

buildDefsTable :: MonadError Error m => Program -> m DefsTable
buildDefsTable p = do
  _funTable <- buildFunTable p
  _structTable <- buildStructTable p
  _classTable <- buildClassTable p
  return DefsTable{..}

-- | Scans the types of all definitions.
buildFunTable :: forall m. MonadError Error m => Program -> m FunTable
buildFunTable (Prog (noTemplateDefs -> ds)) = foldM addDef predefinedFunTable ds
  where
    addDef tbl def =
      case def of
        TopFunDef fdef@(FunDef _ name _ _) ->
          case insertLookup name signature tbl of
            (Nothing, t') -> return t'
            _             -> errDupFunDef name
          where
            signature = typeSignature fdef
        _ -> return tbl

predefinedFunTable :: FunTable
predefinedFunTable = Map.fromList [
  (Ident "printInt", (Void, [Int]))
  , (Ident "printInt_", (Void, [Int]))
  , (Ident "printDouble", (Void, [Double]))
  , (Ident "printDouble_", (Void, [Double]))
  , (Ident "printString", (Void, [String]))
  , (Ident "printString_", (Void, [String]))
  , (Ident "readInt", (Int, []))
  , (Ident "readDouble", (Double, []))
  ]


buildStructTable :: forall m. MonadError Error m => Program -> m StructTable
buildStructTable (Prog (noTemplateDefs -> ds)) = foldM addStruct mempty ds
  where
    addStruct tbl (StructDef name items) = do
      when (name `Map.member` tbl) (errDupStructDef name)
      structInfo <- foldM addItem mempty items
      return $ Map.insert name structInfo tbl
        where
          addItem t (DeclItem (noTemplate -> ty) idents) = foldM (addField ty) t idents
          addField :: Type -> Map Ident Type -> Ident -> m (Map Ident Type)
          addField ty t ident =
            case insertLookup ident ty t of
              (Nothing, t') -> return t'
              _             -> errDupField ident
    addStruct tbl _ = return tbl

buildDefMap :: Program -> Map Ident TopDef
buildDefMap (Prog ds) = foldr addTop mempty ds
  where addTop t = Map.insert iden t
          where
            def = case t of
              TemplateDef _ d -> d
              NoTemplateDef d -> d
            iden = case def of
              TypeDef _real alias                          -> alias
              TopFunDef (FunDef _ fun _ _)                 -> fun
              TopFunDef (FunDefTemplate ty tyvars arr fun _ _) -> fun
              StructDef struct _                           -> struct
              ClassDef cl _                                -> getClassName cl

-- | True if it has no type variables in it. The first argument is the set of
-- defined names, i.e. structs, classes and typedefs.
isConcrete :: Set Ident -> TypeTemplate -> Bool
isConcrete known ty = all (`Set.member`known) [ i | CustomTy i <- universeBi ty ]

isClass :: DefsTable -> Ident -> Bool
isClass defs i = Map.member i (_classTable defs)

isStruct :: DefsTable -> Ident -> Bool
isStruct defs i = Map.member i (_structTable defs)
