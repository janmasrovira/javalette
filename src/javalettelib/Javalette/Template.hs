{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
{-# LANGUAGE ViewPatterns        #-}
module Javalette.Template (expandTemplates) where


import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Generics.Uniplate.Data
import           Data.List
import           Data.Map                       (Map)
import qualified Data.Map                       as Map
import           Data.Maybe
import           Data.Set                       (Set)
import qualified Data.Set                       as Set
import           Extra
import           Javalette.Common
import           Javalette.Error
import           Javalette.Grammar.AbsJavalette
import           Safe.Exact


data TEnv = TEnv {
  _tdefsTable     :: Map Ident TopDef
  , _templateDeps :: Map Ident (Map Ident (Set [TypeTemplate]))
  }

-- | A concrete type is guaranteed to not have template type variables in it.
newtype ConcreteType = Concrete { unConcrete :: TypeTemplate }
  deriving (Show, Eq, Ord)

-- | Expands and removes all the templates in a program.
expandTemplates :: MonadError Error m => Program -> m Program
expandTemplates p@(Prog ds) = do
  let tenv@TEnv{..} = buildTEnv p
      concr = [ (i, concretetys) | a <- Map.elems _templateDeps
                , (i, setty) <- Map.assocs a
                , concretetys <- mapMaybe (mapM (mkConcreteMay known)) (Set.toList setty) ]
      known = Map.keysSet _tdefsTable
      subTemplatesMay TemplateDef{}     = Nothing
      subTemplatesMay x@NoTemplateDef{} = Just (subTemplatesByIdents known x)
      removeTemplates = mapMaybe subTemplatesMay
  insts <- flip runReaderT tenv (flip execStateT mempty (mapM_ expandTrans concr))
  let newDefs = [ subTypeVars def tys | (i, tyset) <- Map.assocs insts
                                    , tys <- Set.toList tyset
                                    , let def = fromJust (Map.lookup i _tdefsTable) ]
  return $ Prog (removeTemplates (newDefs ++ ds))


subTypeVars :: TopDef -> [ConcreteType] -> TopDef
subTypeVars (TemplateDef vars def) tys =
  let varAssigs = Map.fromList (zipExact vars tys)
      allSubs def =
        transformBi subsClassField
        $ transformBi subsFnDef
        $ transformBi subsTypeTemplate def
      subsTypeTemplate :: TypeTemplate -> TypeTemplate
      subsTypeTemplate t
        | NoTemplate (CustomTy tyvar) <- t
        , Just (unConcrete -> ty) <- Map.lookup tyvar varAssigs
        = ty
        | otherwise = t
      subsFnDef :: FnDef -> FnDef
      subsFnDef f
        | FunDef (CustomTy tyvar) fun args b <- f
        , Just (unConcrete -> ty) <- Map.lookup tyvar varAssigs = case ty of
            NoTemplate ty      -> FunDef ty fun args b
            Template i tys arr -> FunDefTemplate i tys arr fun args b
        | otherwise = f
      subsClassField :: ClassItem -> ClassItem
      subsClassField t
        | ClassField (CustomTy tyvar) fs <- t
        , Just (unConcrete -> ty) <- Map.lookup tyvar varAssigs = case ty of
            NoTemplate ty        -> ClassField ty fs
            Template i tvars arr -> ClassFieldTemplate i tvars arr fs
        | otherwise = t
  in NoTemplateDef $ case allSubs def of
    TopFunDef (FunDef ret fun args b) ->
      TopFunDef (FunDef ret (qualifyIdent fun tys) args b)
    TopFunDef (FunDefTemplate ret tyvars arr fun args b) ->
      TopFunDef (FunDefTemplate ret tyvars arr (qualifyIdent fun tys) args b)
    ClassDef (NoParent i) defs ->
      ClassDef (NoParent (qualifyIdent i tys)) defs
    ClassDef{} -> error "template subclasses are not supported"
    StructDef i fields ->
      StructDef (qualifyIdent i tys) fields
    TypeDef{} -> error "template typedefs are not supported"
subTypeVars _ _ = error "not a template definition"


-- top down recursion, use descendBi
subTemplatesByIdents :: Set Ident -> TopDef -> TopDef
subTemplatesByIdents known def =
  let
    subsTempl :: TypeTemplate -> TypeTemplate
    subsTempl t
      | Template i ts arr <- t =
          NoTemplate (arrayToks (qualifyIdent i (conc ts)) arr)
      | NoTemplate ty <- t = NoTemplate $ descendBi subsTempl ty
      | otherwise = t
    subsAppTempl :: Expr -> Expr
    subsAppTempl e
      | EAppTemplate i ts args <- e = EApp (qualifyIdent i (conc ts)) args
      | otherwise = e
    subsFnRetTempl :: FnDef -> FnDef
    subsFnRetTempl f
      | FunDefTemplate ret tyvars arr fun args b <- f =
          FunDef (arrayToks (qualifyIdent ret (map (mkConcrete known) tyvars)) arr) fun args b
      | otherwise = f
    subsFieldTemplate :: ClassItem -> ClassItem
    subsFieldTemplate f
      | ClassFieldTemplate iden tyvars arr fs <- f =
          ClassField (arrayToks (qualifyIdent iden (map (mkConcrete known) tyvars)) arr) fs
      | otherwise = f
    conc = map (mkConcrete known)
  in descendBi subsTempl
     (transformBi subsAppTempl
      (transformBi subsFieldTemplate
       (transformBi subsFnRetTempl def)))


qualifyIdent :: Ident -> [ConcreteType] -> Ident
qualifyIdent (Ident s) ctys =
  Ident (s ++ "." ++ intercalate "$" (map typeToIdentStr tys))
  where tys = map unConcrete ctys

buildTEnv :: Program -> TEnv
buildTEnv p =
  TEnv{..}
  where
    _templateDeps = buildTemplateDeps p
    _tdefsTable = buildDefMap p


-- Map fun/class/struct (Map fun/class/struct (Set [Type]))
buildTemplateDeps :: Program -> Map Ident (Map Ident (Set [TypeTemplate]))
buildTemplateDeps p = foldr addInstance mempty . findTemplates <$> buildDefMap p
  where
    findTemplates x = [ (custy, tys) | Template custy tys _ <- universeBi x ] ++
                      [ (fun, tys) | EAppTemplate fun tys _ <- universeBi x ] ++
                      [ (custy, tys) | ClassFieldTemplate custy tys arr _ <- universeBi x ]
    addInstance (i, tys) = Map.insertWith Set.union i (Set.singleton tys)

templateTypeVars :: (MonadError Error m, MonadReader TEnv m) => Ident -> m [Ident]
templateTypeVars i = do
  td <- fromMaybeM (errTemplate $ "definition not found: " ++ show i)
        (Map.lookup i <$> asks _tdefsTable)
  case td of
    TemplateDef tvars _ -> return tvars
    _                   -> errTemplate $ "definition is not a template\n" ++ show td

expandTrans :: forall m.
  (MonadError Error m, MonadReader TEnv m
  , MonadState (Map Ident (Set [ConcreteType])) m)
  => (Ident, [ConcreteType]) -> m ()
expandTrans (i, ts) = do
  s <- fromMaybe mempty . Map.lookup i <$> get
  unless (Set.member ts s) $ do
    let s' = Set.insert ts s
    modify (Map.insert i s')
    vars <- templateTypeVars i
    varAssigs <- fromMaybeM (errTemplate "wrong template arity")
                 (return (Map.fromList <$> zipExactMay vars ts))
    knownCTys <- Map.keysSet <$> asks _tdefsTable
    let
      mapReplaceVars :: [TypeTemplate] -> m [ConcreteType]
      mapReplaceVars = mapM (fmap (mkConcrete knownCTys) . replaceVars)
      replaceVars = transformBiM replaceVar
      replaceVar t@(NoTemplate (CustomTy tyvar))
        | Just (unConcrete -> ct) <- Map.lookup tyvar varAssigs = return ct
        | isConcrete knownCTys t = return t
        | otherwise = errTemplate $ "unbound type variable: " ++ show tyvar
      replaceVar a = return a
    deps <- fromMaybeM (errTemplate "no deps")
      (Map.lookup i <$> asks _templateDeps)
    x <- sequence [ (i,) <$> concreteInst | (i, tyset) <- Map.assocs deps
              , tempInst <- Set.toList tyset
              , let concreteInst = mapReplaceVars tempInst] -- [(Ident, [ConcreteType])]
    mapM_ expandTrans x

-- | Replace with mkConcrete = const Concrete after debugging.
mkConcrete :: Set Ident -> TypeTemplate -> ConcreteType
mkConcrete defs t = fromMaybe (error $ "not a concrete type: " ++ show t
                                ++ "\nKnown types: " ++ show defs)
                    . mkConcreteMay defs $ t

mkConcreteMay :: Set Ident -> TypeTemplate -> Maybe ConcreteType
mkConcreteMay defs ty
  | isConcrete defs ty = Just (Concrete ty)
  | otherwise = Nothing
