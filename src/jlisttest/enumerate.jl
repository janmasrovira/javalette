template <A, B> class Pair {
  A fst;
  B snd;
  void setFst(A a) {
    fst = a;
  }
  void setSnd(B b) {
    snd = b;
  }
  A getFst() {
    return fst;
  }
  B getSnd() {
    return snd;
  }
}

  template <A, B>
  Pair:<A, B> mkPair(A a, B b) {
    Pair<A, B> p = new Pair<A, B>;
    p.setFst(a);
    p.setSnd(b);
    return p;
  }
void printPairs(Pair<int,int>[] ps) {
  printString_("[");
  if (ps.length > 0) {
    int i = 0;
    while (i < ps.length) {
      printPair(ps[i]);
      i++;
      if (i < ps.length) printString_(", ");
    }
  }
  printString("]");
}


void printPair(Pair<int,int> p) {
  printString_("(");
  printInt_(p.getFst());
  printString_(", ");
  printInt_(p.getSnd());
  printString_(")");
}
template <T> Pair:<int, T>[] enumerate(T[] a) {
  Pair<int, T>[] enum = new Pair<int, T>[a.length];
  int i = 0;
  for (T ai : a) {
    enum[i] = mkPair:<int, T>(i, ai);
    i++;
  }
  return enum;
}


int main() {
  int[] as = <int> [4, 12, 9].toArray();
  printPairs(enumerate:<int>(as));
  return 0;
}
