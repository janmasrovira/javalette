template <T>
List:<List<T>> powerSet(List<T> s) {
  if (s.empty()) return <List<T>>[ <T>[ ] ];
  T x = s.head();
  List<T> t = s.iterator();
  t.vpop();
  List<List<T>> x0 = powerSet:<T>(t);
  List<List<T>> x1 = powerSet:<T>(t);
  List<List<T>> it = x1.iterator();
  while (!it.empty()) {
    it.head().cons(x);
    it.vpop();
  }
  x1.concat(x0);
  return x1;
}

void printSets(List<List<int>> p) {
  List<List<int>> iti = p.iterator();
  while (!iti.empty()) {
    List<int> itj = iti.pop().iterator();
    printString_("{");
    while(!itj.empty()) {
      printInt_(itj.pop());
      if (!itj.empty()) printString_(", ");
    }
    printString("}");
  }
}

int main() {
  int n = 5;
  List<int> l = [1..n];
  List<List<int>> p = powerSet:<int>(l);
  printString_("size: "); printInt(p.length());
  printSets(p);
  return 0;
}
