void printIntList(List<int> l) {
  List<int> it = l.iterator();
  printString_("[");
  while (!it.empty()) {
    printInt_(it.pop());
    if (!it.empty()) printString_(", ");
  }
  printString("]");
}

template<T>
List:<T> concatLists(List<List<T>> ls) {
  List<T> r = <T>[ ];
  List<List<T>> it = ls.iterator();
  while (!it.empty()) r.concat(it.pop());
  return r;
}

template <T>
List:<List<T>> permutations(List<T> l) {
  if (l.empty()) return <List<T>> [l];
  return concatLists:<List<int>> ([ List<List<T>> : pc |
           int i <- [0..l.length() - 1]
           , List<T> c = l.shallowCopy()
           , T x = c.remove(i)
           , List<List<T>> pc = [ List<int> :
                                      with pci { pci.cons(x);  }
                                  | List<T> pci <- permutations:<T>(c) ]
           ]);
}

int main() {
  int n = 4;
  for (List<int> p : permutations:<int>([1..n]).toArray())
    printIntList(p);
  return 0;
}
