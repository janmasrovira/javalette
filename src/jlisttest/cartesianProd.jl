template <A, B> class Pair {
  A fst;
  B snd;
  void setFst(A a) {
    fst = a;
  }
  void setSnd(B b) {
    snd = b;
  }
  A getFst() {
    return fst;
  }
  B getSnd() {
    return snd;
  }
}

template <A, B>
Pair:<A, B> newPair(A a, B b) {
  Pair<A, B> p = new Pair<A, B>;
  p.setFst(a);
  p.setSnd(b);
  return p;
}

void printPair(Pair<int,int> p) {
  printString_("(");
  printInt_(p.getFst());
  printString_(", ");
  printInt_(p.getSnd());
  printString_(")");
}

template <A, B>
Pair:<A, B>[] cartesianProd(A[] as, B[] bs) {
  Pair<A, B>[] res = new Pair<A, B>[as.length * bs.length];
  int i = 0;
  for (A a : as) {
    for (B b : bs) {
      res[i] = newPair:<A,B>(a, b);
      i++;
    }
  }
  return res;
}

void printPairs(Pair<int,int>[] ps) {
  printString_("[");
  if (ps.length > 0) {
    int i = 0;
    while (i < ps.length) {
      printPair(ps[i]);
      i++;
      if (i < ps.length) printString_(", ");
    }
  }
  printString("]");
}

int main() {
  int[] as = <int>[1, 4, 5, 7].toArray();
  int[] bs = <int>[2, 12, 9].toArray();
  Pair<int,int>[] prod = cartesianProd:<int,int>(as, bs);
  printPairs(prod);
  return 0;
}
