void printIntList(List<int> l) {
  List<int> it = l.iterator();
  printString_("[");
  while (!it.empty()) {
    printInt_(it.pop());
    if (!it.empty()) printString_(", ");
  }
  printString("]");
}

// T \in {int, double}
template <T>
List:<T> qsort(List<T> nums) {
  if (nums.length() > 1) {
    T piv = nums.pop();
    List<T> small, big;
    small = [ T : x | T x <- nums, if x < piv ] ;
    big = [ T : x | T x <- nums, if x >= piv ] ;
    small = qsort:<T>(small);
    big = qsort:<T>(big);
    big.cons(piv);
    small.concat(big);
    return small;
  }
  return nums;
}

int main() {
  List<int> nums = <int>[4,4,5,0,32,8,6,3,54] ;
  printString_("unsorted: "); printIntList(nums);
  nums = qsort:<int>(nums);
  printString_("sorted:   "); printIntList(nums);
  return 0;
}
