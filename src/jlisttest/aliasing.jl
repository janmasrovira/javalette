int main() {
  List<int> lst1 = <int>[1,2,3];
  List<int> lst2 = lst1; // aliasing
  printInt(lst1.length()); // 3
  int void_ = lst1.pop();
  printInt(lst2.length()); // 2
  printString("----");

  lst1 = <int>[1,2,3];
  lst2 = lst1.iterator();
  printInt(lst1.length()); // 3
  void_ = lst1.pop();
  printInt(lst2.length()); // 3
  printString("----");

  lst1 = <int>[1,2,3];
  lst2 = lst1.iterator();
  printInt(lst1.length()); // 3
  void_ = lst1.remove(lst1.length() - 1);
  printInt(lst2.length()); // 2
  printString("----");


  lst1 = <int>[1,2,3];
  lst2 = lst1.shallowCopy();
  printInt(lst1.length()); // 3
  void_ = lst1.remove(lst1.length() - 1);
  printInt(lst2.length()); // 3
  printString("----");

  List<List<int>> lst3;
  lst3 = <List<int>>[ <int>[1] ];
  List<List<int>> lst4 = lst3.shallowCopy();
  lst3.head().vpop();
  printInt(lst4.head().length()); // 0
  printString("----");

  return 0;
}
