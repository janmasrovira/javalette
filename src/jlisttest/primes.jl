void printIntList(List<int> l) {
  List<int> it = l.iterator();
  printString_("[");
  while (!it.empty()) {
    printInt_(it.pop());
    if (!it.empty()) printString_(", ");
  }
  printString("]");
}


int main() {
  List<int> primes = <int>[ ];
  int n = 2;
  while (n < 50) {
    boolean b = true;
    List<int> it = primes.iterator();
    while (b && !it.empty() && it.head()*it.head() <= n)
      b = n%it.pop() != 0;
    if (b) primes.append(n);
    n++;
  }
  printIntList(primes);
  return 0;

}
