template <A, B> class Pair {
  A fst;
  B snd;
  void setFst(A a) {
    fst = a;
  }
  void setSnd(B b) {
    snd = b;
  }
  A getFst() {
    return fst;
  }
  B getSnd() {
    return snd;
  }
}

template <A, B>
Pair:<A, B> newPair(A a, B b) {
  Pair<A, B> p = new Pair<A, B>;
  p.setFst(a);
  p.setSnd(b);
  return p;
}

void printPair(Pair<int,int> p) {
  printString_("(");
  printInt_(p.getFst());
  printString_(": ");
  printInt_(p.getSnd());
  printString_(")");
}

template<T>
List:<Pair<int,T>>[] sparseArray(T[][] a) {
  List<Pair<int,T>>[] s = new List<Pair<int,T>>[a.length];
  int i = 0;
  while (i < a.length) {
    int j = a[0].length - 1;
    s[i] = new List<Pair<int,T>>;
    while (j >= 0) {
      if (a[i][j] != 0) {
        Pair<int,T> p = new Pair<int,T>;
        p.setFst(j);
        p.setSnd(a[i][j]);
        List<Pair<int,T>> si = s[i];
        si.cons(p);
      }
      j--;
    }
    i++;
  }
  return s;
}

void printSparseArray(List<Pair<int,int>>[] sa) {
  int i = 0;
  while (i < sa.length){
    List<Pair<int,int>> it = sa[i].iterator();
    if (!it.empty()) {
      printString_("row ");
      printInt_(i);
      printString_(": ");
      while (!it.empty()) {
        printPair(it.pop());
        if (!it.empty()) printString_(", ");
      }
      printString("");
    }
    i++;
  }
}

int main() {
  List<Pair<int,int>> l;
  int[][] a = new int[100][100];
  a[12][65] = 1;
  a[12][78] = 2;
  a[12][45] = 3;
  a[18][90] = 1;
  a[79][0] = 5;
  a[79][99] = 9;
  printSparseArray(sparseArray:<int>(a));
  return 0;
}
