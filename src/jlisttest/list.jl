template<T> class Node {
  T elem;
  Node<T> next;

Node:<T> getNext() {
    return next;
  }

  void setNext(Node<T> l) {
    next = l;
  }

  T getElem() {
    return elem;
  }

  void setElem(T x) {
    elem = x;
  }
}

template <T> class List {
  Node<T> first;

  void vpop() {
    first = first.getNext();
  }

Node:<T> getPtr() {
  return first;
}

Node:<T> ptrAt(int ix) {
  Node<T> r = first;
    while (ix > 0) {
      ix--;
      r = r.getNext();
    }
    return r;
}

  void concat(List<T> l) {
    if (self.empty()) {
      first = l.getPtr();
      return;
    }
    Node<T> last = self.ptrAt(self.length() - 1);
    last.setNext(l.getPtr());
  }

  void set(int ix, T x) {
    Node<T> p = self.ptrAt(ix);
    p.setElem(x);
  }

  T at(int ix) {
    Node<T> p = self.ptrAt(ix);
    return p.getElem();
  }

  int length() {
    Node<T> tmp = first;
    int l = 0;
    while (tmp != (Node<T>)null) {
      l++;
      tmp = tmp.getNext();
    }
    return l;
  }

  boolean empty() {
    return 0 == self.length();
  }


  T remove(int ix) {
    T r;
    if (ix == 0) {
      r = first.getElem();
      first = first.getNext();
    }
    else {
      Node<T> prev = self.ptrAt(ix - 1);
      r = prev.getNext().getElem();
      prev.setNext(prev.getNext().getNext());
    }
    return r;
  }

  void insert(int ix, T x) {
    Node<T> xnode = new Node<T>;
    xnode.setElem(x);
    if (ix == 0) {
      xnode.setNext(first);
      first = xnode;
    }
    else {
      Node<T> p = self.ptrAt(ix - 1);
      xnode.setNext(p.getNext());
      p.setNext(xnode);
    }
  }

  void cons(T x) {
    self.insert(0, x);
  }

  T pop() {
    return self.remove(0);
  }

  void append(T d) {
    self.insert(self.length(), d);
  }

  T head() {
    return first.getElem();
  }

List:<T> shallowCopy() {
    List<T> c = new List<T>;
    T[] a = self.toArray();
    int i = a.length - 1;
    while (i >= 0) {
      c.cons(a[i]);
      i--;
    }
    return c;
  }

  T[] toArray() {
    List<T> it = self.iterator();
    T[] a = new T [self.length()];
    int i = 0;
    while (i < a.length) {
      a[i] = it.pop();
      i++;
    }
    return a;
  }

List:<T> iterator() {
    List<T> l = new List<T>;
    l.setPtr(self.getPtr());
    return l;
  }

  void setPtr(Node<T> ptr) {
    first = ptr;
  }

}

int main() {
  List<double> l = new List<double>;
  return 0;
}

// List:<int> staticEnum(int a, int b) {
//   List<int> l = new List<int>;
//   while (a <= b) {
//     l.append(a);
//     a++;
//   }
//   return l;
// }


