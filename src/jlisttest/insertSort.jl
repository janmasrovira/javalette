void printDoubleList(List<double> l) {
  List<double> it = l.iterator();
  printString_("[");
  while (!it.empty()) {
    printDouble_(it.pop());
    if (!it.empty()) printString_(", ");
  }
  printString("]");
}


// T \in {int, double}
template<T>
void insertsort(List<T> l) {
  int s = 1;
  while (s < l.length()) {
    T x = l.remove(s);
    List<T> it = l.iterator();
    int t = 0;
    while (t < s && it.pop() < x) t++;
    l.insert(t, x);
    s++;
  }
}

int main() {
  List<double> lst = <double>[2.4, 3.0, 0.1, 7.5, 5.9, 5.2, 9.7, 4.3];
  printString_("unsorted: "); printDoubleList(lst);
 insertsort:<double>(lst);
  printString_("sorted:   "); printDoubleList(lst);
  return 0;
}
