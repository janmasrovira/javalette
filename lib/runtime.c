#include <stdio.h>
#include <stdlib.h>

typedef unsigned char byte;

void printInt_(int x) {
  printf("%d", x);
}

void printDouble_(double d) {
  printf("%.1lf", d);
}

void printString_(char* str) {
  printf("%s", str);
}

void printInt(int x) {
  printf("%d\n", x);
}

void printDouble(double d) {
  printf("%.1lf\n", d);
}

void printString(char* str) {
  printf("%s\n", str);
}

int readInt() {
  int r;
  scanf("%d", &r);
  return r;
}

double readDouble() {
  double r;
  scanf("%lf", &r);
  return r;
}

byte* mycalloc(int k, int bytesPerElem) {
  return calloc(k, bytesPerElem);
}

byte* mymalloc(int bytes) {
  return malloc(bytes);
}

void myfree(byte* ptr) {
  free(ptr);
}
